import React from "react";
import "./App.css";
import SignIn from "./Components/LoginForm";

import { Route } from "react-router-dom";
import SignUp from "./Components/signUpForm";
import ForgotPassword from "./Components/ForgotPassword";
import TokenForPassword from "./Components/TokenPage";
import ChangePassword from "./Components/ChangePasswordPage";
import WholeSaleRegForm from "./Components/wholesalers/wholesalerRegForm";
import WholesalerTable from "../src/Components/wholesalers/allWholesalerTable";
import SingleWholesaler from "../src/Components/wholesalers/singleWholesalerTable";
import SellerHub from "../src/Components/Hubs/sellerHub";
import ProfileHub1 from "../src/Components/Hubs/profileHub1";
import SettingsHub from "../src/Components/Hubs/settingsHub";
import BankDeatilsHub from "../src/Components/Hubs/bankDetailsHub";
import ProfileDropHub from "../src/Components/Hubs/profileDropHub";
import NotificationHub from "../src/Components/Hubs/newNotificationHub";
import StoreSignUp from "../src/Components/StorSignUp";
import AccountDetails from "../src/Components/AccountDetails";
import ProfileHub from "../src/Components/Hubs/ProfileHub";
import productVariantHub from "../src/Components/Hubs/productVariantHub";
function App() {
  return (
    <div className="App">
      {/* <Navbar /> */}
      <Route path="/login" component={SignIn} />
      <Route path="/signup" component={SignUp} />
      <Route path="/forgotpassword" component={ForgotPassword} />
      <Route path="/tokenpage" component={TokenForPassword} />
      <Route path="/changepassword" component={ChangePassword} />
      <Route path="/wholesaler" component={WholeSaleRegForm} />
      <Route path="/wholesalertable" component={WholesalerTable} />
      <Route path="/singlewholesaler" component={SingleWholesaler} />
      <Route path="/sellerhub" component={SellerHub} />
      <Route path="/profilehub1" component={ProfileHub1} />
      <Route path="/settingshub" component={SettingsHub} />
      <Route path="/bankdetailshub" component={BankDeatilsHub} />
      <Route path="/profiledrophub" component={ProfileDropHub} />
      <Route path="/notificationhub" component={NotificationHub} />
      <Route path="/storesignup" component={StoreSignUp} />
      <Route path="/accountdetails" component={AccountDetails} />
      <Route path="/profilehub" component={ProfileHub} />
      <Route path="/productvarianthub" component={productVariantHub} />
    </div>
  );
}

export default App;
