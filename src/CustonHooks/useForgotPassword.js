import { useState, useEffect } from "react";
import axios from "axios";

const useForgotPassword = (initialValues, validate) => {
  const [values, setValues] = useState(initialValues);
  const [errors, setErrors] = useState({});
  const [isSubmitted, setIsSubmitted] = useState(false);

  useEffect(() => {
    if (isSubmitted) {
      const noErrors = Object.keys(errors).length === 0;
      if (noErrors) {
        axios
          .post("/forgotpassword", {
            status: "success",
            email: values,
            comment: "This is email gotten on the forgotpassword page"
          })
          .then(response => {
            console.log("user already registered");
          })
          .catch(err => {
            console.log(err);
          });
        setIsSubmitted(false);
      } else {
        console.log(errors);
        setIsSubmitted(false);
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [errors]);

  const handleChange = e => {
    setValues({ ...values, [e.target.name]: e.target.value });
  };

  const handleValidate = e => {
    const validationErrors = validate(values);
    setErrors(validationErrors);
  };

  const handleSubmit = e => {
    e.preventDefault();
    const validationErrors = validate(values);
    setErrors(validationErrors);
    setIsSubmitted(true);
  };

  return [
    values,
    handleChange,
    handleSubmit,
    errors,
    handleValidate,
    isSubmitted
  ];
};

export default useForgotPassword;
