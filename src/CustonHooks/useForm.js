import { useState, useEffect } from "react";
import axios from "axios";

const useForm = (initialValues, validate) => {
  const [values, setValues] = useState(initialValues);
  const [errors, setErrors] = useState({});
  const [isSubmitted, setIsSubmitted] = useState(false);
  const [backendErrorMessage, setBackendErrorMessage] = useState({});

  useEffect(() => {
    if (isSubmitted) {
      const noErrors = Object.keys(errors).length === 0;
      if (noErrors) {
        console.log(values);
        const USER_INFO = {
          firstName: values.firstname,
          lastName: values.lastname,
          email: values.email,
          password: values.password,
          passwordConfirmation: values.confirm_password,
          phone: values.phone,
          pageFrom: values.pageFrom,
          address: values.address_line,
          city: values.city,
          country: values.country,
          institution: values.institution,
          state: values.state,
          onCampus: values.oncampus
        };
        console.log(USER_INFO);
        axios
          .post("https://vasiti-api.herokuapp.com/api/auth/signup ", USER_INFO)
          .then(response => {
            console.log("response : ", response);
          })
          .catch(err => {
            console.log("Error : ", err.response.data.errors);
            setBackendErrorMessage(err.response.data.errors);
          });
        setIsSubmitted(false);
      } else {
        console.log(errors);
        setIsSubmitted(false);
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [errors]);

  const handleChange = e => {
    setValues({ ...values, [e.target.name]: e.target.value });
  };

  const handleValidate = e => {
    const validationErrors = validate(values);
    setErrors(validationErrors);
  };

  const handleSubmit = e => {
    e.preventDefault();
    const validationErrors = validate(values);
    setErrors(validationErrors);
    setIsSubmitted(true);
  };

  return [
    values,
    handleChange,
    handleSubmit,
    errors,
    handleValidate,
    isSubmitted,
    backendErrorMessage
  ];
};

export default useForm;
