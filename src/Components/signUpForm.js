import React from "react";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import useMediaQuery from "@material-ui/core/useMediaQuery";

import Hidden from "@material-ui/core/Hidden";
import Link from "@material-ui/core/Link";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import { withStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import { FormControl, MenuItem, Select } from "@material-ui/core";
import backgroundImage from "./signupimage.png";
import Logo from "../Components/wholesalers/icons/Vasiti-Logo-black 2@1x.png";
import Logo2 from "../Components/wholesalers/icons/Vasiti-Logo-black 1.png";

import useForm from "../CustonHooks/useForm";
import validateForm from "../Functions/validate";
import "./Css/main.css";



const useStyles = makeStyles(theme => ({
  root: { flexGrow: 1 },
  mainContainer: {
    background: `url(${backgroundImage})`,
    maxWidth: "100vw",
    minHeight: "100vh",
    backgroundRepeat: "no-repeat",
    backgroundSize: "cover",
    backgroundPosition: "center",
    padding: "10px 0 20px 0",
    display: "flex",
    "@media only screen and (max-width: 599px)": {
      backgroundImage: "none",
      padding: "0 5px"
    }
  },

  logo: {
    "@media only screen and (max-width: 599px)": {
      marginBottom: "0px"
    }
  },
  paper: {
    marginTop: "20px",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    background: "#fff",
    padding: "64px 10px 53px 10px",
    [theme.breakpoints.down("xs")]: {
      padding: "0px !important"
    }
  },
  avatar: {
    margin: theme.spacing(3),
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(2),
    fontSize: "14px"
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
    fontSize: "20px"
  },
  textfield: {
    minWidth: "100%",
    marginLeft: 0,
    backgroundColor: "#FAFAFA",
    borderRadius: "5px"
  },
  typography: {
    float: "left",
    fontSize: "14px",
    fontWeight: "500",
    margin: "0 0 0 0",
    color: "black"
  },
  supergrid: {
    "@media only screen and (max-width: 768px)": {
      marginTop: "-50px"
    },
    "@media only screen and (min-width: 960px)": {
      paddingTop: "120px",
      paddingLeft: "-50px"
    }
  },
  select: {
    "& .MuiSelect-selectMenu": {
      height: "1.185rem"
    },
    "& .MuiInputBase-input": {
      padding: "12px 14px 7px 14px",
      display: "flex"
    }
  },
  formControl: {
    margin: "-20px 0 -10px 0",
    minWidth: "100%",
    backgroundColor: "#FAFAFA",
    borderBottom: "none !important",
    borderRadius: "5px",
    "& .MuiInput-underline:hover:not(.Mui-disabled):before": {
      borderBottom: "none"
    },
    "& .MuiInput-underline:after": {
      borderBottom: "none !important"
    },
    "& .MuiInput-underline:before": {
      borderBottom: "none !important"
    }
  }
}));
const CssTextField = withStyles({
  root: {
    "& label.Mui-focused": {
      color: "grey",
      fontSize: 10
    },
    "& ::placeholder": { fontSize: "14px" },

    "&:hover": {
      borderColor: "#FF5C00"
    },
    "& .MuiInput-underline:after": {
      borderBottom: "none !important"
    },
    "& .MuiInput-underline:before": {
      borderColor: "#FF5C00"
    },
    "& .MuiInput-underline:hover:not(.Mui-disabled):before": {
      borderColor: "#FF5C00"
    },
    "& .MuiOutlinedInput-root": {
      "& fieldset": {
        border: "none",
        opacity: "60%"
      },
      "&:hover fieldset": {
        borderColor: "#FF5C00"
      },
      "&.Mui-focused fieldset": {
        border: "1px solid #FF5C00"
      }
    }
  }
})(TextField);

const SignUp = () => {
  const classes = useStyles();
  const matches = useMediaQuery("(min-width:601px)");

  console.log(matches);
  const [labelWidth] = React.useState(0);
  // React.useEffect(() => {
  //   setLabelWidth(inputLabel.current.offsetWidth);
  // }, []);

  const errorMessageStyle = {
    color: "red",
    opacity: "70%",
    fontSize: "10px",
    fontWidth: "bolder",
    fontStyle: "oblique"
  };

  const USER_INFO = {
    firstname: "",
    lastname: "",
    phone: "",
    pageFrom: "vendor",
    email: "",
    password: "",
    confirm_password: "",
    address_line: "",
    city: "",
    state: "",
    country: "",
    institution: "",
    oncampus: ""
  };

  const [values, handleChange, handleSubmit, errors] = useForm(
    USER_INFO,
    validateForm
  );

  return (
    <>
      <Grid container className={classes.mainContainer}>
        <Grid
          className={classes.logoParent}
          item
          md={6}
          sm={12}
          xs={12}
          style={{
            padding: matches ? "50px 0 0 8rem" : "0 0 0 4rem",
            textAlign: "left"
          }}
        >
          <img
            className={classes.logoParent}
            src={matches ? Logo : Logo2}
            alt="logo"
            style={{
              position: "relative",
              top: "30px"
            }}
          />
          {/* takimg welcome text */}
          <Hidden smDown>
            <h1
              style={{
                fontSize: "48px",
                color: "white",
                display: "flex",
                alignContent: "flex-end",
                position: "relative",
                top: "20%"
              }}
            >
              Welcome to
              <br />
              Vasiti Seller Hub.
            </h1>
          </Hidden>
        </Grid>
        <Grid item md={6} sm={12} xs={12}>
          <Grid item xs={12} sm={12} md={12}>
            <Container
              component="main"
              maxWidth="sm"
              style={{
                backgroundColor: "#fff",
                borderRadius: "10px"
              }}
            >
              <CssBaseline />

              <div className={classes.paper} style={{ margin: "50px 0 0 0" }}>
                <Typography
                  component="h1"
                  variant="h5"
                  style={{
                    color: "#FF5C00",
                    fontWeight: "bolder",
                    margin: "5px 0 5px 0"
                  }}
                >
                  Sign up
                </Typography>
                <form
                  className={classes.form}
                  noValidate
                  onSubmit={handleSubmit}
                >
                  <Grid container spacing={2}>
                    <Grid item xs={12} sm={6}>
                      <Typography
                        variant="body2"
                        className={classes.typography}
                      >
                        Firstname
                      </Typography>
                      <CssTextField
                        variant="outlined"
                        size="small"
                        className={classes.textfield}
                        autoComplete="firstname"
                        name="firstname"
                        required
                        fullWidth
                        id="firstName"
                        placeholder="Enter your Firstname"
                        autoFocus
                        value={values.firstname}
                        onChange={handleChange}
                        // onKeyUp={handleValidate}
                      />
                      {errors.firstname && (
                        <span style={errorMessageStyle}>
                          {errors.firstname}
                        </span>
                      )}
                    </Grid>
                    <Grid item xs={12} sm={6}>
                      <Typography
                        variant="body2"
                        className={classes.typography}
                      >
                        Lastname
                      </Typography>
                      <CssTextField
                        variant="outlined"
                        size="small"
                        className={classes.textfield}
                        required
                        fullWidth
                        id="lastName"
                        placeholder="Enter your Lastname"
                        name="lastname"
                        value={values.lastname}
                        onChange={handleChange}
                        // onKeyUp={handleValidate}
                        autoComplete="lastname"
                      />
                      {errors.lastname && (
                        <span style={errorMessageStyle}>{errors.lastname}</span>
                      )}
                    </Grid>
                    <Grid item xs={12} sm={6}>
                      <Typography
                        variant="body2"
                        className={classes.typography}
                      >
                        Email
                      </Typography>
                      <CssTextField
                        variant="outlined"
                        size="small"
                        className={classes.textfield}
                        required
                        fullWidth
                        id="email"
                        placeholder="Enter your Email"
                        name="email"
                        value={values.email}
                        onChange={handleChange}
                        // onKeyUp={handleValidate}
                        autoComplete="email"
                      />
                      {errors.email && (
                        <span style={errorMessageStyle}>{errors.email}</span>
                      )}
                    </Grid>
                    <Grid item xs={12} sm={6}>
                      <Typography
                        variant="body2"
                        className={classes.typography}
                      >
                        Phone Number
                      </Typography>
                      <CssTextField
                        size="small"
                        variant="outlined"
                        className={classes.textfield}
                        required
                        fullWidth
                        id="phone"
                        placeholder="Enter your Phone Number"
                        name="phone"
                        value={values.phone}
                        onChange={handleChange}
                        autoComplete="phone"
                        // onKeyUp={handleValidate}
                      />
                      {errors.phone && (
                        <span style={errorMessageStyle}>{errors.phone}</span>
                      )}
                    </Grid>
                    <Grid item xs={12} sm={6}>
                      <Typography
                        variant="body2"
                        className={classes.typography}
                      >
                        Password
                      </Typography>
                      <CssTextField
                        variant="outlined"
                        size="small"
                        className={classes.textfield}
                        required
                        fullWidth
                        type="password"
                        id="password"
                        placeholder="Enter your Password"
                        name="password"
                        value={values.password}
                        onChange={handleChange}
                        autoComplete="new-password"
                        // onKeyUp={handleValidate}
                      />
                      {errors.password && (
                        <span style={errorMessageStyle}>{errors.password}</span>
                      )}
                    </Grid>
                    <Grid item xs={12} sm={6}>
                      <Typography
                        variant="body2"
                        className={classes.typography}
                      >
                        Confirm Password
                      </Typography>
                      <CssTextField
                        variant="outlined"
                        size="small"
                        className={classes.textfield}
                        required
                        fullWidth
                        type="password"
                        id="confirm-password"
                        placeholder="Enter your Password"
                        name="confirm_password"
                        value={values.confirm_password}
                        onChange={handleChange}
                        // onKeyUp={handleValidate}
                        autoComplete="confirm_password"
                      />
                      <p>
                        {errors.confirm_password && (
                          <span style={errorMessageStyle}>
                            {errors.confirm_password}
                          </span>
                        )}
                      </p>
                      <p>
                        {errors.equality && (
                          <span style={errorMessageStyle}>
                            {errors.equality}
                          </span>
                        )}
                      </p>
                    </Grid>

                    <Grid item xs={12} sm={6}>
                      <Typography
                        variant="body2"
                        className={classes.typography}
                      >
                        Are you on campus
                      </Typography>
                      <FormControl
                        size="small"
                        className={classes.formControl}
                        style={{ marginTop: "0.9px", marginLeft: "-2px" }}
                      >
                        <Select
                          labelId="institution"
                          id="demo-simple-select-outlined"
                          placeholder="Are you on campus"
                          name="oncampus"
                          value={values.oncampus}
                          onChange={handleChange}
                          labelWidth={labelWidth}
                          className={classes.select}
                        >
                          <MenuItem value="" disabled>
                            <em>Are you on campus</em>
                          </MenuItem>
                          <MenuItem value="yes">Yes</MenuItem>
                          <MenuItem value="no">No</MenuItem>
                        </Select>
                      </FormControl>
                      <p>
                        {errors.oncampus && (
                          <span style={errorMessageStyle}>
                            {errors.oncampus}
                          </span>
                        )}
                      </p>
                    </Grid>

                    <Grid item xs={12} sm={6}>
                      <Typography
                        variant="body2"
                        className={classes.typography}
                      >
                        Institution
                      </Typography>
                      <FormControl
                        size="small"
                        className={classes.formControl}
                        style={{ marginTop: "0.9px", marginLeft: "-2px" }}
                      >
                        <Select
                          labelId="institution"
                          id="demo-simple-select-outlined"
                          name="institution"
                          value={values.institution}
                          onChange={handleChange}
                          labelWidth={labelWidth}
                          className={classes.select}
                        >
                          <MenuItem value="">
                            <em>None</em>
                          </MenuItem>
                          <MenuItem value="convenant">Convenat</MenuItem>
                          <MenuItem value="University of Lagos">
                            University of Lagos
                          </MenuItem>
                          <MenuItem value="University of Ibadan">
                            University of Ibadan
                          </MenuItem>
                        </Select>
                      </FormControl>
                      <p>
                        {errors.institution && (
                          <span style={errorMessageStyle}>
                            {errors.institution}
                          </span>
                        )}
                      </p>
                    </Grid>

                    <Grid item xs={12} sm={6}>
                      <Typography
                        variant="body2"
                        className={classes.typography}
                      >
                        Address
                      </Typography>
                      <CssTextField
                        size="small"
                        variant="outlined"
                        className={classes.textfield}
                        required
                        fullWidth
                        placeholder="Enter your Address"
                        type="text"
                        id="address-line"
                        name="address_line"
                        value={values.address_line}
                        onChange={handleChange}
                        // onKeyUp={handleValidate}
                        autoComplete="address_line"
                      />
                      {errors.address_line && (
                        <span style={errorMessageStyle}>
                          {errors.address_line}
                        </span>
                      )}
                    </Grid>
                    <Grid item xs={12} sm={6}>
                      <Typography
                        variant="body2"
                        className={classes.typography}
                      >
                        City
                      </Typography>
                      <CssTextField
                        size="small"
                        variant="outlined"
                        className={classes.textfield}
                        required
                        fullWidth
                        placeholder="Enter your City"
                        type="text"
                        id="city"
                        name="city"
                        value={values.city}
                        onChange={handleChange}
                        // onKeyUp={handleValidate}
                        autoComplete="city"
                      />
                      {errors.city && (
                        <span style={errorMessageStyle}>{errors.city}</span>
                      )}
                    </Grid>
                    <Grid item xs={12} sm={6}>
                      <Typography
                        variant="body2"
                        className={classes.typography}
                      >
                        State
                      </Typography>
                      <CssTextField
                        size="small"
                        variant="outlined"
                        className={classes.textfield}
                        required
                        fullWidth
                        id="state"
                        type="text"
                        placeholder="Enter your State"
                        name="state"
                        value={values.state}
                        onChange={handleChange}
                        // onKeyUp={handleValidate}
                        autoComplete="city"
                      />
                      {errors.state && (
                        <span style={errorMessageStyle}>{errors.state}</span>
                      )}
                    </Grid>

                    <Grid item xs={12} sm={6}>
                      <Typography
                        variant="body2"
                        className={classes.typography}
                      >
                        Country
                      </Typography>
                      <CssTextField
                        size="small"
                        variant="outlined"
                        className={classes.textfield}
                        required
                        fullWidth
                        id="country"
                        type="text"
                        placeholder="Enter your Country"
                        name="country"
                        value={values.country}
                        onChange={handleChange}
                        // onKeyUp={handleValidate}
                        autoComplete="country"
                      />
                      {errors.country && (
                        <span style={errorMessageStyle}>{errors.country}</span>
                      )}
                    </Grid>
                  </Grid>
                  <Button
                    type="submit"
                    fullWidth
                    variant="contained"
                    style={{ backgroundColor: "#FF5C00", color: "white" }}
                    className={classes.submit}
                  >
                    CREATE ACCOUNT
                  </Button>
                  <Grid container justify="flex-end">
                    <Grid item>
                      <Link href="/login" variant="body2">
                        Already have an account? Sign in
                      </Link>
                    </Grid>
                  </Grid>
                </form>
              </div>
            </Container>
          </Grid>
        </Grid>
      </Grid>
    </>
  );
};

export default SignUp;
