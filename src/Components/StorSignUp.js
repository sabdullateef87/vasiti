import React from "react";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import Link from "@material-ui/core/Link";
import Grid from "@material-ui/core/Grid";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import { withStyles } from "@material-ui/core/styles";
import Hidden from "@material-ui/core/Hidden";
import Box from "@material-ui/core/Box";
import useMediaQuery from "@material-ui/core/useMediaQuery";

import backgroundImage from "./signupimage.png";
import Logo from "../Components/wholesalers/icons/Vasiti-Logo-black 2@1x.png";
import Logo2 from "../Components/wholesalers/icons/Vasiti-Logo-black 1.png";
import useLoginForm from "../CustonHooks/useLoginForm";
import validateLoginForm from "../Functions/LoginValidate";

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {"Copyright © "}
      <Link color="inherit" href="https://material-ui.com/">
        Your Website
      </Link>{" "}
      {new Date().getFullYear()}
      {"."}
    </Typography>
  );
}

const CssTextField = withStyles({
  root: {
    "& label.Mui-focused": {
      color: "grey",
      fontSize: 10
    },
    "& ::placeholder": { fontSize: "14px" },

    "&:hover": {
      borderColor: "#FF5C00"
    },
    "& .MuiInput-underline:after": {
      borderBottom: "none !important"
    },
    "& .MuiInput-underline:before": {
      borderColor: "#FF5C00"
    },
    "& .MuiInput-underline:hover:not(.Mui-disabled):before": {
      borderColor: "#FF5C00"
    },
    "& .MuiOutlinedInput-root": {
      "& fieldset": {
        border: "none",
        opacity: "60%"
      },
      "&:hover fieldset": {
        borderColor: "#FF5C00"
      },
      "&.Mui-focused fieldset": {
        border: "1px solid #FF5C00"
      }
    }
  }
})(TextField);

const useStyles = makeStyles(theme => ({
  paper: {
    marginTop: "20px",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    background: "#fff",
    padding: "10px 0 0 0"
  },
  mainContainer: {
    background: `url(${backgroundImage})`,
    maxWidth: "100vw",
    minHeight: "100vh",
    backgroundRepeat: "no-repeat",
    backgroundSize: "cover",
    backgroundPosition: "center",
    padding: "10px 0 20px 0",
    display: "flex",
    "@media only screen and (max-width: 768px)": {
      backgroundImage: "none"
    }
  },

  logo: {
    "@media only screen and (max-width: 768px)": {
      marginBottom: "0px"
    }
  },
  avatar: {
    margin: theme.spacing(3),
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1)
  },
  submit: {
    margin: theme.spacing(3, 0, 2)
  },
  textfield: {
    minWidth: "100%",
    marginLeft: 0,
    backgroundColor: "#FAFAFA",
    borderRadius: "5px"
  },
  typography: {
    float: "left",
    fontSize: "14px",
    fontWeight: "bold",
    margin: "0 0 2px 0"
  }
}));

export default function StoreSignUp() {
  const classes = useStyles();
  const matches = useMediaQuery("(min-width:768px)");

  const USER_INFO = {
    pageFrom: "vendor",
    email: "",
    password: ""
  };
  const [
    values,
    handleChange,
    handleSubmit,
    errors,
    handleValidate
  ] = useLoginForm(USER_INFO, validateLoginForm);
  const errorMessageStyle = {
    color: "red",
    opacity: "70%",
    fontSize: "10px",
    fontWidth: "bolder",
    fontStyle: "oblique"
  };
  return (
    <>
      <Grid container className={classes.mainContainer}>
        <Grid
          item
          md={6}
          sm={12}
          xs={12}
          style={{ padding: "20px 0 0 8rem", textAlign: "left" }}
        >
          <img
            src={matches ? Logo : Logo2}
            alt="logo"
            style={{
              position: "relative",
              top: "30px"
            }}
          />
          {/* takimg welcome text */}
          <Hidden smDown>
            <h1
              style={{
                fontSize: "48px",
                color: "white",
                display: "flex",
                alignContent: "flex-end",
                position: "relative",
                top: "20%"
              }}
            >
              Welcome to
              <br />
              Vasiti Seller Hub.
            </h1>
          </Hidden>
        </Grid>
        <Grid item md={6} sm={12} xs={12}>
          <Grid item xs={12} sm={12} md={12}>
            <Container
              component="main"
              maxWidth="xs"
              style={{ backgroundColor: "#fff", borderRadius: "10px" }}
            >
              <CssBaseline />
              <div className={classes.paper} style={{ margin: "50px 0 0 0" }}>
                <Typography
                  component="h1"
                  variant="h5"
                  style={{
                    color: "#FF5C00",
                    margin: "50px 0 0 0",
                    fontWeight: "bold",
                    fontSize: "30px"
                  }}
                >
                  Setup Your Store
                </Typography>
                <Typography
                  component="h1"
                  variant="subtitle2"
                  style={{ color: "black", margin: "0 0 10px 0" }}
                >
                  Create a virtual store for your products
                </Typography>
                <form className={classes.form} noValidate>
                  <Grid
                    container
                    spacing={2}
                    justify="center"
                    alignItems="center"
                  >
                    <Grid item xs={12} sm={12}>
                      <Typography
                        variant="body2"
                        className={classes.typography}
                      >
                        Store Name
                      </Typography>
                      <CssTextField
                        size="small"
                        className={classes.textfield}
                        autoComplete="storename"
                        name="storename"
                        variant="outlined"
                        required
                        fullWidth
                        id="storename"
                        placeholder="Enter store name"
                        autoFocus
                      />
                    </Grid>
                    <Grid item xs={12} sm={12}>
                      <Typography
                        variant="body2"
                        className={classes.typography}
                      >
                        General Category
                      </Typography>
                      <CssTextField
                        size="small"
                        className={classes.textfield}
                        variant="outlined"
                        required
                        fullWidth
                        id="genaralcategory"
                        placeholder="Enter general category"
                        name="genaralcategory"
                        autoComplete="lastname"
                      />
                    </Grid>
                    <Grid item xs={12} sm={12}>
                      <Typography
                        variant="body2"
                        className={classes.typography}
                      >
                        Description
                      </Typography>
                      <CssTextField
                        size="small"
                        className={classes.textfield}
                        variant="outlined"
                        required
                        fullWidth
                        id="description"
                        placeholder="Description"
                        name="description"
                        autoComplete="description"
                      />
                    </Grid>
                  </Grid>
                  <Button
                    type="submit"
                    fullWidth
                    variant="contained"
                    style={{ backgroundColor: "#FF5C00", color: "white" }}
                    className={classes.submit}
                  >
                    CREATE STORE
                  </Button>
                  <Grid style={{ position: "relative", top: "-20px" }}>
                    <p>SKIP STEP</p>
                  </Grid>
                </form>
              </div>
            </Container>
          </Grid>
        </Grid>
      </Grid>
    </>
  );
}
