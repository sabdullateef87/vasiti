import React from "react";
import { Link } from "react-router-dom";

const Navbar = () => {
  return (
    <>
      <Link to="/login">Sign In</Link>

      <Link to="/signup">SignUp</Link>

      <Link to="/wholesaler">wholesaler</Link>
    </>
  );
};
export default Navbar;
