import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import NotificationsNoneIcon from "@material-ui/icons/NotificationsNone";
import Badge from "@material-ui/core/Badge";
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles(theme => ({
  typography: {
    float: "left",
    fontSize: "14px",
    fontWeight: "500",
    color: "black"
  },
  logo: {
    marginTop: "-10px",
    [theme.breakpoints.down("xs")]: {
      width: "24px !important",
      height: "24px !important",
      marginTop: "-1.9px !important"
    }
  },
  accountDetails: {
    [theme.breakpoints.down("xs")]: {
      fontSize: "14px !important"
      // marginTop: "50px !important"
    }
  },
  name: {
    fontWeight: "bold",
    marginLeft: "10px",
    [theme.breakpoints.down("sm")]: {
      fontWeight: 600,
      fontSize: "14px !important",
      marginLeft: "5px !important"
    }
  },
  logoContainer: {
    [theme.breakpoints.down("sm")]: {
      justifyContent: "flex-end !important"
    },
    [theme.breakpoints.up("sm")]: {
      justifyContent: "flex-end !important"
    }
  },
  badge: {
    [theme.breakpoints.down("sm")]: {
      marginRight: "20px !important"
    },
    [theme.breakpoints.up("sm")]: {
      marginRight: "40px !important"
    }
  }
}));

export default function Subheader({ name }) {
  const pageName = name => {
    return { name };
  };
  const classes = useStyles();

  return (
    <>
      <Grid
        item
        xs={12}
        style={{ display: "flex", justifyContent: "space-between" }}
      >
        <Grid item xs={12} md={9} sm={9}>
          <h1
            className={classes.accountDetails}
            style={{
              color: "#FF5C00",
              float: "left",
              fontWeight: "bold",
              fontSize: "24px",
              marginTop: "18px"
            }}
          >
            {name}
          </h1>
        </Grid>

        <Grid
          item
          xs={12}
          md={2}
          sm={3}
          className={classes.logoContainer}
          style={{
            display: "flex",
            justifyContent: "space-between",
            marginTop: "18px"
          }}
        >
          <div style={{ marginLeft: "20px" }} className={classes.badge}>
            <Badge badgeContent={4} color="error">
              <NotificationsNoneIcon />
            </Badge>
          </div>
          <div style={{ display: "flex" }}>
            <span
              className={classes.logo}
              style={{
                backgroundColor: "#FAEAE1",
                width: "40px",
                height: "40px",
                borderRadius: "100%",
                color: "#FF5C00",
                display: "flex",
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <Typography variant="caption">S</Typography>
            </span>

            <span className={classes.name} style={{}}>
              Sunday
            </span>
          </div>
        </Grid>
      </Grid>
    </>
  );
}
