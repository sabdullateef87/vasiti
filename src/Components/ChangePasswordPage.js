import React, { useState } from "react";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import Link from "@material-ui/core/Link";

import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import { withStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import LockOpenIcon from "@material-ui/icons/LockOpen";

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {"Copyright © "}
      <Link color="inherit" href="https://material-ui.com/">
        Your Website
      </Link>{" "}
      {new Date().getFullYear()}
      {"."}
    </Typography>
  );
}

const useStyles = makeStyles(theme => ({
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center"
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1)
  },
  submit: {
    margin: theme.spacing(3, 0, 2)
  }
}));

const CssTextField = withStyles({
  root: {
    "& label.Mui-focused": {
      color: "grey"
    },
    "& .MuiInput-underline:after": {
      borderBottomColor: "green"
    },
    "& .MuiOutlinedInput-root": {
      "& fieldset": {
        borderColor: "grey",
        opacity: "60%"
      },
      "&:hover fieldset": {
        borderColor: "#FF5C00"
      },
      "&.Mui-focused fieldset": {
        borderColor: "#FF5C00"
      }
    }
  }
})(TextField);

export default function ChangePassword() {
  const classes = useStyles();
  const [value, setValue] = useState({
    newpassword: "",
    confirm_new_password: ""
  });
  const [message, setMessage] = useState(false);

  const handleSubmit = e => {
    e.preventDefault();
    if (value.newpassword[0] !== value.confirm_new_password[0]) {
      setMessage(true);
    }
  };

  const handleChange = e => {
    setValue({ ...value, [e.target.name]: [e.target.value] });
  };

  //   const handleValidate = e => {
  //       if(value.password !== valu)
  //   };

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOpenIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Change Passcode
        </Typography>
        <form className={classes.form} noValidate onSubmit={handleSubmit}>
          <CssTextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="email"
            label="New Password"
            name="newpassword"
            autoComplete="newpassword"
            type="password"
            autoFocus
            onChange={handleChange}
            onKeyUp={() => {
              setMessage(false);
            }}
          />
          <CssTextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="confirm_new_password"
            label="Confirm New Password"
            name="confirm_new_password"
            type="password"
            autoComplete="confirm_new_password"
            autoFocus
            onChange={handleChange}
            onKeyUp={() => {
              setMessage(false);
            }}
          />
          {message && <span>Password do not match</span>}

          <Button
            style={{ backgroundColor: "#FF5C00", color: "white" }}
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            <Link color="inherit" href="#">
              Change Password
            </Link>{" "}
          </Button>
        </form>
      </div>
      <Box mt={8}>
        <Copyright />
      </Box>
    </Container>
  );
}
