const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
const passwordRegex = /(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[#$^+=!*()@%&]).{8,10}/;
const nameRegex = /^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*$/;
const wholesaleValidate = values => {
  let errors = {};
  //Firstname validation
  if (
    !values.firstname ||
    values.firstname.length === 0 ||
    values.firstname === ""
  ) {
    errors.firstname = "Required";
  } else if (!nameRegex.test(values.firstname)) {
    errors.firstname = "Invalid";
  }

  //Lastname Validation
  if (!values.lastname) {
    errors.lastname = "Required";
  } else if (!nameRegex.test(values.lastname)) {
    errors.lastname = "Invalid";
  }

  //phonenumber validation
  if (!values.phone) {
    errors.phone = "Required";
  } else if (values.phone.length < 11) {
    errors.phone = "Number too short";
  }

  //email Validation
  if (!values.email) {
    errors.email = "Required";
  } else if (!emailRegex.test(values.email)) {
    errors.email = "Invalid";
  }
  //password validation
  if (!values.password) {
    errors.password = "Required";
  } else if (!passwordRegex.test(values.password)) {
    errors.password =
      "password must be greater than 8 characters and e.g (1Abdoliumytu@)";
  }
  // else if (values.password.length < 6) {
  //   errors.password = "password must be more than 6 characters";
  // }

  //address line validation
  if (!values.address_line) {
    errors.address_line = "Required";
  }

  //city validation
  if (!values.city) {
    errors.city = "Required";
  }

  //state validation
  if (!values.state) {
    errors.state = "Required";
  }
  //countr validation
  if (!values.country) {
    errors.country = "Required";
  }

  return errors;
};

export default wholesaleValidate;
