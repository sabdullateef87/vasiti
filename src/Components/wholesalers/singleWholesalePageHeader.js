import React from "react";
import Badge from "@material-ui/core/Badge";
import PersonIcon from "@material-ui/icons/Person";
import Logo from "./icons/img_90947.png";
const SingleWholesalePageHeader = ({ product }) => {
  const myStyles = {
    root: {
      float: "left",
      display: "flex",
      alignItems: "center",
      justifyContent: "center",
      margin: "30px 0 30px 0"
    },
    personicon: {
      fontSize: "32",
      color: "#FF5C00"
    },
    iconparent: {
      backgroundColor: "grey",
      width: "50px",
      height: "50px",
      display: "flex",
      alignItems: "center",
      justifyContent: "center",
      borderRadius: "50%",
      margin: "0 0 0 30px"
    }
  };
  return (
    <>
      <div style={myStyles.root}>
        <span style={myStyles.iconparent}>
          <PersonIcon style={myStyles.personicon} />
        </span>
        <div style={{ margin: "0 0 0 40px" }}>
          <span style={{ fontSize: "28px" }}>
            <strong>Suleiman Abdullateef</strong>
          </span>
          <br />
          <span>Number 10, Ogijo, Ikorodu, Lagos.</span>
          <br />
          <span>abdullateef@jujo.com</span>
          <br />
          <span>+2348188410567</span>
        </div>
      </div>

      <Badge
        badgeContent={product.length}
        color="primary"
        style={{ margin: "50px 0 0 0" }}
      >
        <span>
          <img
            src={Logo}
            style={{
              width: "50px"
            }}
          />
          <div>Total Product</div>
        </span>
      </Badge>
    </>
  );
};
export default SingleWholesalePageHeader;
