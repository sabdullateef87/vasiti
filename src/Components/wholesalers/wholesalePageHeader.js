import React from "react";

import PeopleAltIcon from "@material-ui/icons/PeopleAlt";
import PersonAddIcon from "@material-ui/icons/PersonAdd";
import Badge from "@material-ui/core/Badge";
import Button from "@material-ui/core/Button";

const WholesalerPageHeader = ({ wholesaler }) => {
  return (
    <>
      <div>
        <Badge
          badgeContent={wholesaler.length}
          color="secondary"
          style={{
            color: "#FF5C00",
            fontSize: 56,
            float: "left",
            margin: "20px 0 20px 20px"
          }}
        >
          <PeopleAltIcon
            style={{
              color: "#FF5C00",
              fontSize: 32,
              float: "left",
              margin: "0 0 20px 20px"
            }}
          />
        </Badge>

        <Button
          variant="contained"
          color="grey"
          style={{
            float: "left",
            margin: "10px 0 0 40px",
            backgroundColor: "#FF5C00"
          }}
          startIcon={
            <PersonAddIcon
              style={{
                color: "white",
                fontSize: 32,
                float: "left",
                margin: ""
              }}
            />
          }
        >
          <span style={{ color: "white" }}>Add User</span>
        </Button>
      </div>
    </>
  );
};

export default WholesalerPageHeader;
