import React from "react";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";

import Link from "@material-ui/core/Link";
import Grid from "@material-ui/core/Grid";
import Box from "@material-ui/core/Box";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import { withStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import {
  FormControl,
  InputLabel,
  InputBase,
  MenuItem,
  Select
} from "@material-ui/core";

import wholesaleValidate from "../wholesalers/Functions/wholesaleValidate";
import useWholesaleHook from "../wholesalers/customHooks/useWholesaleHook";

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {"Copyright © "}
      <Link color="inherit" href="https://material-ui.com/">
        Your Website
      </Link>{" "}
      {new Date().getFullYear()}
      {"."}
    </Typography>
  );
}

const useStyles = makeStyles(theme => ({
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center"
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(3)
  },
  submit: {
    margin: theme.spacing(3, 0, 2)
  },
  textfield: {
    minWidth: "100%",
    marginLeft: 0
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: "100%"
  }
}));
const CssTextField = withStyles({
  root: {
    "& label.Mui-focused": {
      color: "grey",
      fontSize: 10
    },
    "& .MuiInput-underline:after": {
      borderBottomColor: "green"
    },
    "& .MuiOutlinedInput-root": {
      "& fieldset": {
        borderColor: "grey",
        opacity: "60%"
      },
      "&:hover fieldset": {
        borderColor: "#FF5C00"
      },
      "&.Mui-focused fieldset": {
        borderColor: "#FF5C00"
      }
    }
  }
})(TextField);

const BootstrapInput = withStyles(theme => ({
  root: {
    // "label + &": {
    //   marginTop: theme.spacing(2)
    // }
  },
  input: {
    borderRadius: 4,
    position: "relative",
    backgroundColor: theme.palette.background.paper,
    border: "1px solid #ced4da",
    fontSize: 12,
    padding: "10px 26px 10px 12px",
    marginLeft: "-10px",
    marginBottom: "-20px",
    height: "30px",
    transition: theme.transitions.create(["border-color", "box-shadow"]),
    // Use the system font instead of the default Roboto font.
    fontFamily: [
      "-apple-system",
      "BlinkMacSystemFont",
      '"Segoe UI"',
      "Roboto",
      '"Helvetica Neue"',
      "Arial",
      "sans-serif",
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"'
    ].join(","),
    "&:focus": {
      borderRadius: 4,
      borderColor: "#FF5C00",
      boxShadow: "0 0 0 0.2rem rgba(0,123,255,.25)"
    }
  }
}))(InputBase);

const WholeSaleRegForm = () => {
  const classes = useStyles();
  const inputLabel = React.useRef(null);
  const [labelWidth, setLabelWidth] = React.useState(0);

  const errorMessageStyle = {
    color: "red",
    opacity: "70%",
    fontSize: "10px",
    fontWidth: "bolder",
    fontStyle: "oblique"
  };

  const USER_INFO = {
    firstname: "",
    lastname: "",
    phone: "",
    pageFrom: "wholesaler",
    email: "",
    password: "",
    address_line: "",
    city: "",
    state: "",
    country: ""
  };

  const [
    values,
    handleChange,
    handleSubmit,
    errors,
    handleValidate
  ] = useWholesaleHook(USER_INFO, wholesaleValidate);

  return (
    <Container component="main" maxWidth="sm">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5" style={{ color: "#FF5C00" }}>
          ADD WHOLESALER
        </Typography>
        <form className={classes.form} noValidate onSubmit={handleSubmit}>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <CssTextField
                className={classes.textfield}
                autoComplete="firstname"
                name="firstname"
                variant="outlined"
                required
                fullWidth
                id="firstName"
                label="First Name"
                autoFocus
                value={values.firstname}
                onChange={handleChange}
                // onKeyUp={handleValidate}
              />
              {errors.firstname && (
                <span style={errorMessageStyle}>{errors.firstname}</span>
              )}
            </Grid>
            <Grid item xs={12} sm={6}>
              <CssTextField
                variant="outlined"
                required
                fullWidth
                id="lastName"
                label="Last Name"
                name="lastname"
                value={values.lastname}
                onChange={handleChange}
                // onKeyUp={handleValidate}
                autoComplete="lastname"
              />
              {errors.lastname && (
                <span style={errorMessageStyle}>{errors.lastname}</span>
              )}
            </Grid>
            <Grid item xs={12} sm={6}>
              <CssTextField
                variant="outlined"
                required
                fullWidth
                id="email"
                label="Email Address"
                name="email"
                value={values.email}
                onChange={handleChange}
                // onKeyUp={handleValidate}
                autoComplete="email"
              />
              {errors.email && (
                <span style={errorMessageStyle}>{errors.email}</span>
              )}
            </Grid>
            <Grid item xs={12} sm={6}>
              <CssTextField
                variant="outlined"
                required
                fullWidth
                id="phone"
                label="Phone Number"
                name="phone"
                value={values.phone}
                onChange={handleChange}
                autoComplete="phone"
                // onKeyUp={handleValidate}
              />
              {errors.phone && (
                <span style={errorMessageStyle}>{errors.phone}</span>
              )}
            </Grid>
            <Grid item xs={12} sm={6}>
              <CssTextField
                variant="outlined"
                required
                fullWidth
                type="password"
                id="password"
                label="Password"
                name="password"
                value={values.password}
                onChange={handleChange}
                // onKeyUp={handleValidate}
              />
              {errors.password && (
                <span style={errorMessageStyle}>{errors.password}</span>
              )}
            </Grid>

            <Grid item xs={12} sm={6}>
              <CssTextField
                variant="outlined"
                required
                fullWidth
                label="Address Line"
                type="text"
                id="address-line"
                name="address_line"
                value={values.address_line}
                onChange={handleChange}
                // onKeyUp={handleValidate}
                autoComplete="address_line"
              />
              {errors.address_line && (
                <span style={errorMessageStyle}>{errors.address_line}</span>
              )}
            </Grid>
            <Grid item xs={12} sm={6}>
              <CssTextField
                variant="outlined"
                required
                fullWidth
                label="City"
                type="text"
                id="city"
                name="city"
                value={values.city}
                onChange={handleChange}
                // onKeyUp={handleValidate}
                autoComplete="city"
              />
              {errors.city && (
                <span style={errorMessageStyle}>{errors.city}</span>
              )}
            </Grid>
            <Grid item xs={12} sm={6}>
              <CssTextField
                variant="outlined"
                required
                fullWidth
                id="state"
                type="text"
                label="State"
                name="state"
                value={values.state}
                onChange={handleChange}
                // onKeyUp={handleValidate}
                autoComplete="city"
              />
              {errors.state && (
                <span style={errorMessageStyle}>{errors.state}</span>
              )}
            </Grid>

            <Grid item xs={12} sm={12}>
              <CssTextField
                variant="outlined"
                required
                fullWidth
                id="country"
                type="text"
                label="Country"
                name="country"
                value={values.country}
                onChange={handleChange}
                // onKeyUp={handleValidate}
                autoComplete="country"
              />
              {errors.country && (
                <span style={errorMessageStyle}>{errors.country}</span>
              )}
            </Grid>
          </Grid>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            style={{ backgroundColor: "#FF5C00", color: "white" }}
            className={classes.submit}
          >
            ADD NEW WHOLESALER
          </Button>
        </form>
      </div>
      <Box mt={5}>
        <Copyright />
      </Box>
    </Container>
  );
};

export default WholeSaleRegForm;
