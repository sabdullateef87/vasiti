import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import Pagination from "@material-ui/lab/Pagination";
import CircularProgress from "@material-ui/core/CircularProgress";

import { useState, useEffect } from "react";
import Button from "@material-ui/core/Button";
import axios from "axios";
import WholesalerPageHeader from "../wholesalers/wholesalePageHeader";

const useStyles = makeStyles(theme => ({
  root: {
    width: "100%",
    marginTop: theme.spacing(3),
    overflowX: "auto"
  },
  table: {
    minWidth: 650
  },
  button: {
    margin: "3px",
    backgroundColor: "#FF5C00",
    color: "white"
  }
}));

export default function WholesalerTable() {
  const [wholesaler, setWholesaler] = useState([
    {
      image: "",
      color: "red",
      size: 24,
      quantity: 25,
      price: "#5000",
      stock_status: ""
    },
    {
      image: "",
      color: "white",
      size: 30,
      quantity: 8,
      price: "#5000",
      stock_status: ""
    },
    {
      image: "",
      color: "red",
      size: 24,
      quantity: 25,
      price: "#5000",
      stock_status: ""
    },
    {
      image: "",
      color: "red",
      size: 24,
      quantity: 25,
      price: "#5000",
      stock_status: ""
    },
    {
      image: "",
      color: "red",
      size: 24,
      quantity: 25,
      price: "#5000",
      stock_status: ""
    },
    {
      image: "",
      color: "red",
      size: 24,
      quantity: 25,
      price: "#5000",
      stock_status: ""
    },
    {
      image: "",
      color: "red",
      size: 24,
      quantity: 25,
      price: "#5000",
      stock_status: ""
    },
    {
      image: "",
      color: "red",
      size: 24,
      quantity: 25,
      price: "#5000",
      stock_status: ""
    },
    {
      image: "",
      color: "red",
      size: 24,
      quantity: 25,
      price: "#5000",
      stock_status: ""
    },
    {
      image: "",
      color: "red",
      size: 24,
      quantity: 25,
      price: "#5000",
      stock_status: ""
    },
    {
      image: "",
      color: "red",
      size: 24,
      quantity: 25,
      price: "#5000",
      stock_status: ""
    },
    {
      image: "",
      color: "red",
      size: 24,
      quantity: 25,
      price: "#5000",
      stock_status: ""
    },
    {
      image: "",
      color: "red",
      size: 24,
      quantity: 25,
      price: "#5000",
      stock_status: ""
    },
    {
      image: "",
      color: "red",
      size: 24,
      quantity: 25,
      price: "#5000",
      stock_status: ""
    },
    {
      image: "",
      color: "red",
      size: 24,
      quantity: 25,
      price: "#5000",
      stock_status: ""
    },
    {
      image: "",
      color: "red",
      size: 24,
      quantity: 25,
      price: "#5000",
      stock_status: ""
    },
    {
      image: "",
      color: "red",
      size: 24,
      quantity: 25,
      price: "#5000",
      stock_status: ""
    },
    {
      image: "",
      color: "red",
      size: 24,
      quantity: 25,
      price: "#5000",
      stock_status: ""
    },
    {
      image: "",
      color: "red",
      size: 24,
      quantity: 25,
      price: "#5000",
      stock_status: ""
    }
  ]);

  const [products, setProducts] = useState([]);
  const [loading, setLoading] = useState(false);
  const [users, setUsers] = useState([]);

  const [currentpage, setCurrentPage] = useState(1);
  const [productsPerPage, setProductsPerPage] = useState(5);

  //replace wholesaler with user because user is already set to the data gotten from the endpoint
  //uncomment the code below and comment or delete the settimeout code

  // useEffect(() => {
  //   const fetchProduct = async () => {
  //     setLoading(true);
  //     const res = await axios.get("/jdjhxfjhjkhjksd");
  //     setUsers(res.data);
  //     setLoading(false);
  //   };
  //   fetchProduct();
  // }, []);

  useEffect(() => {
    setLoading(true);
    setTimeout(function() {
      setLoading(false);
    }, 3000);
  }, []);
  const indexOfLastProduct = productsPerPage * currentpage;
  const indexOfFirstProduct = indexOfLastProduct - productsPerPage;
  const currentProduct = wholesaler.slice(
    indexOfFirstProduct,
    indexOfLastProduct
  );
  //const currentProduct = products.slice(indexOfFirstProduct, indexOfLastProduct);
  const paginate = (event, value) => setCurrentPage(value);

  const classes = useStyles();

  return (
    <>
      <WholesalerPageHeader wholesaler={wholesaler} />
      {loading ? (
        <div>
          <CircularProgress
            style={{ margin: "20% 0 0 -15%", color: "#FF5C00" }}
          />
        </div>
      ) : (
        <div>
          <div style={{ padding: "30px" }}>
            <Paper className={classes.root}>
              <Table className={classes.table}>
                <TableHead>
                  <TableRow>
                    <TableCell>Name</TableCell>
                    <TableCell align="center">
                      <strong>Email</strong>
                    </TableCell>
                    <TableCell align="center">
                      <strong>Contact&nbsp;</strong>
                    </TableCell>
                    <TableCell align="center">
                      <strong>&nbsp;Address</strong>
                    </TableCell>
                    <TableCell align="center">
                      <strong>Action&nbsp;</strong>
                    </TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {currentProduct.map(row => (
                    <TableRow key={row.id}>
                      <TableCell component="th" scope="row">
                        {`${row.firstname} ${row.lastname}`}
                      </TableCell>
                      <TableCell align="center">{row.email}</TableCell>
                      <TableCell align="center">{row.phone}</TableCell>
                      <TableCell align="center">{row.address}</TableCell>
                      <TableCell align="center">
                        <Button variant="contained" className={classes.button}>
                          View
                        </Button>
                        <Button variant="contained" className={classes.button}>
                          Edit
                        </Button>
                        <Button variant="contained" className={classes.button}>
                          Remove
                        </Button>
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </Paper>
          </div>
          <Pagination
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              margin: "40px 0 0 0"
            }}
            count={Math.ceil(wholesaler.length / productsPerPage)}
            shape="rounded"
            page={currentpage}
            onChange={paginate}
          />
        </div>
      )}
    </>
  );
}
