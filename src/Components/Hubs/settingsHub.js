import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import Alert from "@material-ui/lab/Alert";
import Container from "@material-ui/core/Container";
import NotificationsNoneIcon from "@material-ui/icons/NotificationsNone";
import Badge from "@material-ui/core/Badge";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import Link from "@material-ui/core/Link";
import Box from "@material-ui/core/Box";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import Typography from "@material-ui/core/Typography";
import { withStyles } from "@material-ui/core/styles";

import Subheader from "../Subheader/Subheader";

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1
  },
  mainContainer: {
    display: "flex"
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(2),
    fontSize: "14px"
  },
  paper: {
    marginTop: "20px",
    background: "#fff",
    padding: "10px 0 0 0"
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
    fontSize: "14px",
    "&.MuiButton-contained": {
      boxShadow: "none"
    },
    [theme.breakpoints.down("sm")]: {
      fontSize: "14px !important"
    }
  },
  textfield: {
    minWidth: "100%",
    marginLeft: 0,
    backgroundColor: "#FAFAFA",
    borderRadius: "5px"
  },
  typography: {
    float: "left",
    fontSize: "14px",
    fontWeight: "500",
    margin: "0 0 0 0",
    color: "black"
  }
}));
const CssTextField = withStyles({
  root: {
    "& label.Mui-focused": {
      color: "grey",
      fontSize: 10
    },
    "& ::placeholder": { fontSize: "14px" },

    "&:hover": {
      borderColor: "#FF5C00"
    },
    "& .MuiInput-underline:after": {
      borderBottom: "none !important"
    },

    "& .MuiInput-underline:before": {
      borderColor: "#FF5C00"
    },
    "& .MuiInput-underline:hover:not(.Mui-disabled):before": {
      borderColor: "#FF5C00"
    },
    "& .MuiOutlinedInput-root": {
      "& fieldset": {
        border: "none",
        opacity: "60%"
      },
      "&:hover fieldset": {
        borderColor: "#FF5C00"
      },
      "&.Mui-focused fieldset": {
        border: "1px solid #FF5C00"
      }
    }
  }
})(TextField);

export default function SettingsHub() {
  const classes = useStyles();

  return (
    <>
      <Grid container className={classes.mainContainer}>
        <Grid item md={12} sm={12} xs={12}>
          <Alert
            icon={false}
            severity="warning"
            style={{
              backgroundColor: "#FAEAE1",
              border: "1px solid #FF5C00"
            }}
          >
            Your store is not visible yet, go to settings to set up your store
          </Alert>
        </Grid>
        <Subheader name="Settings" />
        <Grid>
          <Container
            component="main"
            maxWidth="xs"
            style={{ backgroundColor: "#fff", borderRadius: "10px" }}
          >
            <CssBaseline />
            <div className={classes.paper} style={{ margin: "20px 0 0 0" }}>
              <Typography
                component="h1"
                variant="h5"
                style={{
                  fontWeight: "500",
                  fontSize: "24px",
                  margin: "5px 0 5px 0",
                  display: "flex",
                  alignItems: "flex-start"
                }}
              >
                Account Settings
              </Typography>

              <hr width="100%" />
              <form className={classes.form} noValidate>
                <div>
                  <Typography variant="body2" className={classes.typography}>
                    Current Password
                  </Typography>
                  <CssTextField
                    className={classes.textfield}
                    size="small"
                    variant="outlined"
                    placeholder="Enter your Current Password"
                    margin="normal"
                    required
                    fullWidth
                    id="email"
                    name="email"
                    autoComplete="email"
                  />
                </div>
                <Typography variant="body2" className={classes.typography}>
                  New Password
                </Typography>
                <CssTextField
                  className={classes.textfield}
                  size="small"
                  variant="outlined"
                  placeholder="Enter your New password"
                  margin="normal"
                  required
                  fullWidth
                  type="password"
                  id="password"
                  name="password"
                />

                <Button
                  type="submit"
                  fullWidth
                  variant="contained"
                  style={{
                    backgroundColor: "white",
                    color: "#FF5C00",
                    border: "1px solid #FF5C00"
                  }}
                  className={classes.submit}
                >
                  CHANGE PASSWORD
                </Button>
              </form>

              <Typography
                component="h1"
                variant="h5"
                style={{
                  fontWeight: "500",
                  fontSize: "24px",
                  margin: "5px 0 5px 0",
                  display: "flex",
                  alignItems: "flex-start"
                }}
              >
                Store Settings
              </Typography>
              <hr width="100%" />
              <form className={classes.form} noValidate>
                <div>
                  <Typography variant="body2" className={classes.typography}>
                    Name of store
                  </Typography>
                  <CssTextField
                    className={classes.textfield}
                    size="small"
                    variant="outlined"
                    placeholder="Enter the name of yout store"
                    margin="normal"
                    required
                    fullWidth
                    type="text"
                    id="storeName"
                    name="storeName"
                    autoComplete="storeName"
                  />
                </div>
                <Typography variant="body2" className={classes.typography}>
                  Store general category
                </Typography>
                <CssTextField
                  className={classes.textfield}
                  size="small"
                  variant="outlined"
                  placeholder="e.g Fashion, Accessories"
                  margin="normal"
                  required
                  fullWidth
                  type="text"
                  id="storeGenaralCategory"
                  name="storeGenaralCategory"
                />
                <Typography variant="body2" className={classes.typography}>
                  Store description
                </Typography>
                <CssTextField
                  className={classes.textfield}
                  size="small"
                  variant="outlined"
                  placeholder="A brief description of your store"
                  margin="normal"
                  required
                  fullWidth
                  type="text"
                  id="storeBriefDescription"
                  name="storeBriefDescription"
                />
                <Button
                  type="submit"
                  fullWidth
                  variant="contained"
                  style={{
                    backgroundColor: "white",
                    color: "#FF5C00",
                    border: "1px solid #FF5C00"
                  }}
                  className={classes.submit}
                >
                  SAVE STORE SETTINGS
                </Button>
              </form>
            </div>
          </Container>
        </Grid>
      </Grid>
    </>
  );
}
