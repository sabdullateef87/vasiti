import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import Alert from "@material-ui/lab/Alert";
import Container from "@material-ui/core/Container";
import NotificationsNoneIcon from "@material-ui/icons/NotificationsNone";
import Badge from "@material-ui/core/Badge";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import Link from "@material-ui/core/Link";
import Box from "@material-ui/core/Box";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import Typography from "@material-ui/core/Typography";
import { withStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    padding: "100px 0 0 0 "
  },
  paper: {
    padding: theme.spacing(0),
    textAlign: "center"
  },
  submit: {
    margin: theme.spacing(3, 0, 2)
  },
  container: {
    backgroundColor: "#E5E5E5",
    padding: "20px"
  }
}));
const CssTextField = withStyles({
  root: {
    "& label.Mui-focused": {
      color: "grey",
      fontSize: 10
    },
    "& .MuiInput-underline:after": {
      borderBottomColor: "green"
    },
    "& .MuiOutlinedInput-root": {
      "& fieldset": {
        borderColor: "grey",
        opacity: "60%"
      },
      "&:hover fieldset": {
        borderColor: "#FF5C00"
      },
      "&.Mui-focused fieldset": {
        borderColor: "#FF5C00"
      }
    }
  }
})(TextField);
export default function ProfileDropHub() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      {/* <CssBaseline /> */}
      <Container maxWidth="md" className={classes.container}>
        <Grid container spacing={3}>
          <Grid item xs={12}>
            <Paper className={classes.paper}>
              <Alert
                icon={false}
                severity="warning"
                style={{
                  backgroundColor: "#FAEAE1",
                  border: "1px solid #FF5C00"
                }}
              >
                Your store is not visible yet, go to settings to set up your
                store
              </Alert>
            </Paper>
          </Grid>
          <Grid item xs={12}>
            <Paper className={classes.paper}>
              <span
                style={{ color: "#FF5C00", float: "left", fontWeight: "bold" }}
              >
                Bank Account Details
              </span>

              <span
                style={{
                  width: "20px",
                  height: "20px",
                  backgroundColor: "#FF5C00",
                  borderRadius: "40%",
                  fontSize: "15px"
                }}
              ></span>

              <span
                style={{
                  float: "right",
                  margin: "5px 0 0 10px",
                  fontWeight: "bold"
                }}
              >
                Sunday
              </span>

              <span
                style={{
                  display: "flex",

                  alignItems: "center",
                  justifyContent: "center",
                  float: "right",
                  margin: "0 0 -10px 40px ",
                  backgroundColor: "#FAEAE1",
                  borderRadius: "50%",
                  padding: "5px",
                  width: "20px",
                  heigh: "20px",
                  color: "#FF5C00",
                  fontWeight: "bold"
                }}
              >
                s
              </span>

              <span>
                <Badge
                  badgeContent={4}
                  color="error"
                  style={{ color: "#FF5C00", float: "right" }}
                >
                  <NotificationsNoneIcon
                    style={{ color: "black", float: "right" }}
                  />
                </Badge>
              </span>
            </Paper>
          </Grid>

          <Grid item xs={12}>
            <Paper
              className={classes.paper}
              style={{
                minHeight: "200px",
                display: "flex",
                padding: "30px 10px 30px 0px",
                //border: "1px solid #F8F8F8  ",
                backgroundColor: "#E5E5E5",
                boxShadow: "none"
              }}
            >
              <Grid item xs={6}>
                <div
                  style={{
                    margin: "0 0 0 0px",
                    border: "1px ridge silver",
                    padding: "20px",
                    borderRadius: "2%",
                    backgroundColor: "#fff"
                  }}
                >
                  <div style={{ margin: "20px 0 0 0px" }}>
                    <CssTextField
                      size="small"
                      style={{ backgroundColor: "#E6E6E6" }}
                      variant="outlined"
                      margin="normal"
                      required
                      fullWidth
                      id="bankname"
                      label="Bank Name"
                      name="bankname"
                      autoComplete="bankname"
                      type="text"
                      autoFocus
                    />
                    <CssTextField
                      size="small"
                      style={{ backgroundColor: "#E6E6E6" }}
                      variant="outlined"
                      margin="normal"
                      required
                      fullWidth
                      id="accountnumber"
                      label="Account Number"
                      name="accountnumber"
                      type="text"
                      autoComplete="accountnumber"
                      autoFocus
                    />
                    <CssTextField
                      size="small"
                      style={{ backgroundColor: "#E6E6E6" }}
                      variant="outlined"
                      margin="normal"
                      required
                      fullWidth
                      id="accountname"
                      label="Account Name"
                      name="accountname"
                      type="text"
                      autoComplete="accountname"
                      autoFocus
                    />

                    <Button
                      style={{
                        backgroundColor: "#FF5C00",
                        color: "white",
                        border: "2px solid #FF5C00",
                        boxShadow: "none"
                      }}
                      type="submit"
                      fullWidth
                      variant="contained"
                      color="primary"
                      className={classes.submit}
                    >
                      <Link color="inherit" href="#">
                        Save Account Details
                      </Link>{" "}
                    </Button>
                  </div>
                </div>
              </Grid>
              <Grid item xs={6}></Grid>
            </Paper>
          </Grid>
        </Grid>
      </Container>
    </div>
  );
}
