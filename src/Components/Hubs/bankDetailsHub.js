import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import Alert from "@material-ui/lab/Alert";
import NotificationsNoneIcon from "@material-ui/icons/NotificationsNone";
import Badge from "@material-ui/core/Badge";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Link from "@material-ui/core/Link";
import Typography from "@material-ui/core/Typography";
import { withStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import CssBaseline from "@material-ui/core/CssBaseline";

import Subheader from "../Subheader/Subheader";

const useStyles = makeStyles(theme => ({
  paper: {
    marginTop: "20px",
    marginRight: "10px",
    display: "flex",
    flexDirection: "column",
    //alignItems: "center",
    background: "#fff",
    padding: "10px 0 0 0"
  },
  mainContainer: {
    flexGrow: 1
  },

  avatar: {
    margin: theme.spacing(3),
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1)
  },
  submit: {
    margin: theme.spacing(3, 0, 2)
  },
  textfield: {
    minWidth: "100%",
    marginLeft: 0,
    backgroundColor: "#FAFAFA",
    borderRadius: "5px"
  },
  typography: {
    float: "left",
    fontSize: "18px",
    fontWeight: "500",
    margin: "0 0 10px 0",
    color: "black",
    [theme.breakpoints.down("xs")]: {
      fontSize: "12px"
    }
  },
  logo: {
    marginTop: "-10px",
    [theme.breakpoints.down("xs")]: {
      width: "24px !important",
      height: "24px !important",
      marginTop: "-1.9px !important"
    }
  },
  accountDetails: {
    [theme.breakpoints.down("xs")]: {
      fontSize: "14px !important",
      marginTop: "50px !important"
    }
  },
  name: {
    fontWeight: "bold",
    marginLeft: "10px",
    [theme.breakpoints.down("sm")]: {
      fontWeight: 600,
      fontSize: "14px !important",
      marginLeft: "5px !important"
    }
  }
}));
const CssTextField = withStyles({
  root: {
    "& label.Mui-focused": {
      color: "grey",
      fontSize: 10
    },
    "& ::placeholder": { fontSize: "14px" },

    "&:hover": {
      borderColor: "#FF5C00"
    },
    "& .MuiInput-underline:after": {
      borderBottom: "none !important"
    },

    "& .MuiInput-underline:before": {
      borderColor: "#FF5C00"
    },
    "& .MuiInput-underline:hover:not(.Mui-disabled):before": {
      borderColor: "#FF5C00"
    },
    "& .MuiOutlinedInput-root": {
      "& fieldset": {
        border: "none",
        opacity: "60%"
      },
      "&:hover fieldset": {
        borderColor: "#FF5C00"
      },
      "&.Mui-focused fieldset": {
        border: "1px solid #FF5C00"
      }
    }
  }
})(TextField);

export default function BankDeatilsHub() {
  const classes = useStyles();

  return (
    <>
      <Grid container className={classes.mainContainer}>
        <Grid item md={12} sm={12} xs={12}>
          <Alert
            icon={false}
            severity="warning"
            style={{
              backgroundColor: "#FAEAE1",
              border: "1px solid #FF5C00"
            }}
          >
            Your store is not visible yet, go to settings to set up your store
          </Alert>
        </Grid>
        <Subheader name="Bank Account Details" />
        <Grid>
          <Container
            component="main"
            maxWidth="xs"
            style={{ backgroundColor: "#fff", borderRadius: "10px" }}
          >
            <CssBaseline />
            <div className={classes.paper}>
              <form className={classes.form} noValidate>
                <Grid
                  container
                  spacing={2}
                  justify="center"
                  alignItems="center"
                >
                  <Grid item xs={12} sm={12}>
                    <Typography variant="body2" className={classes.typography}>
                      Bank Name
                    </Typography>
                    <CssTextField
                      size="medium"
                      className={classes.textfield}
                      autoComplete="bankName"
                      name="bankName"
                      variant="outlined"
                      required
                      fullWidth
                      id="bankName"
                      placeholder="Enter your bank"
                      autoFocus
                    />
                  </Grid>
                  <Grid item xs={12} sm={12}>
                    <Typography variant="body2" className={classes.typography}>
                      Account Number
                    </Typography>
                    <CssTextField
                      size="medium"
                      className={classes.textfield}
                      variant="outlined"
                      required
                      fullWidth
                      id="accoountNumber"
                      placeholder="Enter account number"
                      name="accoountNumber"
                      autoComplete="accountNumber"
                    />
                  </Grid>
                  <Grid item xs={12} sm={12}>
                    <Typography variant="body2" className={classes.typography}>
                      Account Name
                    </Typography>
                    <CssTextField
                      size="medium"
                      className={classes.textfield}
                      variant="outlined"
                      required
                      fullWidth
                      id="accountName"
                      placeholder="Account Name"
                      name="accountName"
                      autoComplete="accountName"
                    />
                  </Grid>
                </Grid>
                <Button
                  type="submit"
                  fullWidth
                  variant="contained"
                  style={{
                    backgroundColor: "#FF5C00",
                    color: "white",
                    fontSize: "18px"
                  }}
                  className={classes.submit}
                >
                  SAVE ACCOUNT DETAILS
                </Button>
                <Grid style={{ position: "relative", top: "-20px" }}>
                  <p>SKIP STEP</p>
                </Grid>
              </form>
            </div>
          </Container>
        </Grid>
      </Grid>
    </>
  );
}
