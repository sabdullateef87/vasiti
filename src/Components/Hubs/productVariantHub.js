import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import { withStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";

import Subheader from "../Subheader/Subheader";
import imgFrame from "../wholesalers/icons/imgFrame.svg";
import AddIcon from "@material-ui/icons/Add";


const useStyles = makeStyles(theme => ({
  paper: {
    background: "#fff",

    width: "100%",
    borderRadius: "20px",
    [theme.breakpoints.down("xs")]: {
      padding: "20px 20px !important",
      marginTop: "10px"
    }
  },
  mainContainer: {
    // padding: "10px 30px",
    flexGrow: 1,
    backgroundColor: "#E4E4E4",
    [theme.breakpoints.down("xs")]: {
      //padding: "0px 10px"
    }
  },

  avatar: {
    margin: theme.spacing(3),
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    // Fix IE 11 issue.
    marginTop: theme.spacing(1)
  },
  submit: {
    margin: "4px 0",
    boxShadow: "none"
  },
  textfield: {
    marginLeft: 0,
    backgroundColor: "#FAFAFA",
    borderRadius: "5px"
  },
  typography: {
    float: "left",
    fontSize: "18px",
    fontWeight: "500",
    color: "black",
    [theme.breakpoints.down("xs")]: {
      fontSize: "12px"
    }
  },
  logo: {
    marginTop: "-10px",
    [theme.breakpoints.down("xs")]: {
      width: "24px !important",
      height: "24px !important",
      marginTop: "-1.9px !important"
    }
  },
  accountDetails: {
    [theme.breakpoints.down("xs")]: {
      fontSize: "14px !important",
      marginTop: "50px !important"
    }
  },
  name: {
    fontWeight: "bold",
    marginLeft: "10px",
    [theme.breakpoints.down("sm")]: {
      fontWeight: 600,
      fontSize: "14px !important",
      marginLeft: "5px !important"
    }
  },
  profileGrid: {
    [theme.breakpoints.down("sm")]: {
      display: "none"
    }
  },
  formContainer: {
    [theme.breakpoints.down("xs")]: {
      padding: "10px !important"
    }
  },
  addVariantPane: {
    [theme.breakpoints.down("xs")]: {
      flexDirection: "column !important",
      marginTop: "0px !important"
    }
  },
  addVariantButton: {
    height: "296px",
    width: "100%",
    [theme.breakpoints.down("xs")]: {
      alignSelf: "unset !important"
    }
  },
  pvText: {
    [theme.breakpoints.down("xs")]: {
      marginLeft: "0px !important",
      fontSize: "14px !important"
    }
  }
}));
const CssTextField = withStyles({
  root: {
    "& label.Mui-focused": {
      color: "grey",
      fontSize: 10
    },
    "& ::placeholder": { fontSize: "14px" },

    "&:hover": {
      borderColor: "#FF5C00"
    },
    "& .MuiInput-underline:after": {
      borderBottom: "none !important"
    },

    "& .MuiInput-underline:before": {
      borderColor: "#FF5C00"
    },
    "& .MuiInput-underline:hover:not(.Mui-disabled):before": {
      borderColor: "#FF5C00"
    },
    "& .MuiOutlinedInput-root": {
      "& fieldset": {
        border: "none",
        opacity: "60%"
      },
      "&:hover fieldset": {
        borderColor: "#FF5C00"
      },
      "&.Mui-focused fieldset": {
        border: "1px solid #FF5C00"
      }
    }
  }
})(TextField);
export default function ProductVariantHub() {
  const classes = useStyles();

  return (
    <>
      {/* <CssBaseline /> */}
      <Grid
        container
        className={classes.mainContainer}
        justify="space-between"
        alignItems="flex-start"
      >
        <Subheader name="Profile" />
        <Grid container justify="space-between">
          <Grid
            item
            xs={12}
            sm={5}
            style={{ display: "flex", alignSelf: "center" }}
          >
            <Paper className={classes.paper} style={{ padding: "40px" }}>
              <Grid item xs={12} md={12} sm={12}>
                <Grid item xs={12} md={12} sm={12} style={{ display: "flex" }}>
                  <h1
                    className={classes.typography}
                    style={{ fontSize: "24px", marginButtom: "10px" }}
                  >
                    Product Details
                  </h1>
                </Grid>
                <form className={classes.form} noValidate>
                  <Typography variant="body2" className={classes.typography}>
                    Name
                  </Typography>
                  <CssTextField
                    size="small"
                    placeholder="Nike Airmax"
                    variant="outlined"
                    margin="normal"
                    required
                    fullWidth
                    id="fullname"
                    name="fullname"
                    autoComplete="fullname"
                    autoFocus
                    className={classes.textfield}
                  />
                  <Typography variant="body2" className={classes.typography}>
                    Category
                  </Typography>
                  <CssTextField
                    size="small"
                    placeholder="Fashion"
                    variant="outlined"
                    margin="normal"
                    required
                    fullWidth
                    name="phone"
                    type="phone"
                    id="phone"
                    autoComplete="phone"
                    className={classes.textfield}
                  />
                  <Typography variant="body2" className={classes.typography}>
                    Sub category
                  </Typography>
                  <CssTextField
                    size="small"
                    placeholder="Mens Shoes"
                    variant="outlined"
                    margin="normal"
                    required
                    fullWidth
                    id="email"
                    name="email"
                    autoComplete="email"
                    autoFocus
                    className={classes.textfield}
                  />
                  <Typography variant="body2" className={classes.typography}>
                    Product Price
                  </Typography>
                  <CssTextField
                    size="small"
                    placeholder="12,000"
                    variant="outlined"
                    margin="normal"
                    required
                    fullWidth
                    id="fullname"
                    name="fullname"
                    autoComplete="fullname"
                    autoFocus
                    className={classes.textfield}
                  />
                  <Typography variant="body2" className={classes.typography}>
                    Discount Price(optional)
                  </Typography>
                  <CssTextField
                    size="small"
                    placeholder="15,000"
                    variant="outlined"
                    margin="normal"
                    required
                    fullWidth
                    id="address"
                    name="address"
                    autoComplete="address"
                    autoFocus
                    className={classes.textfield}
                  />

                  <Button
                    type="submit"
                    size="large"
                    fullWidth
                    variant="contained"
                    style={{
                      backgroundColor: "#FF5C00",
                      color: "white",
                      fontSize: "18px"
                    }}
                    className={classes.submit}
                  >
                    ADD PRODUCT
                  </Button>
                </form>
              </Grid>
            </Paper>
          </Grid>

          <Grid item xs={12} sm={6} md={6}>
            <Grid
              className={classes.pvText}
              xs={12}
              sm={8}
              md={6}
              style={{ display: "flex", marginTop: "20px" }}
            >
              <Typography variant="h5"> Product Variant</Typography>
            </Grid>

            <Grid
              item
              className={classes.addVariantPane}
              xs={12}
              sm={12}
              md={12}
              style={{
                display: "flex",
                justifyContent: "space-between",
                alignItems: "flex-start",
                marginTop: "20px"
              }}
            >
              <Grid item xs={12} sm={8} md={6} style={{ display: "flex" }}>
                <Grid
                  container
                  className={classes.paper}
                  justify="space-between"
                  style={{
                    width: "100%",
                    minHeight: "338px",
                    backgroundColor: "white",
                    borderRadius: "20px",
                    padding: "21px"
                  }}
                >
                  <Grid item xs={12} md={12} sm={12}>
                    <div
                      style={{
                        width: "100%",
                        height: "212px",
                        backgroundColor: "#FAFAFA",
                        marginBottom: "30px",
                        display: "flex",
                        justifyContent: "center",
                        alignItems: "center",
                        borderRadius: "5px"
                      }}
                    >
                      <img src={imgFrame} alt="the frame" />
                    </div>
                  </Grid>

                  <Grid item xs={5} sm={6} md={5}>
                    <Typography variant="body2" className={classes.typography}>
                      Color
                    </Typography>
                    <CssTextField
                      size="small"
                      variant="outlined"
                      required
                      fullWidth
                      id="fullname"
                      name="fullname"
                      autoComplete="fullname"
                      autoFocus
                      className={classes.textfield}
                    />
                  </Grid>
                  <Grid item xs={5} sm={6} md={5}>
                    <Typography variant="body2" className={classes.typography}>
                      Size
                    </Typography>
                    <CssTextField
                      size="small"
                      variant="outlined"
                      required
                      fullWidth
                      id="fullname"
                      name="fullname"
                      autoComplete="fullname"
                      autoFocus
                      className={classes.textfield}
                    />
                  </Grid>
                  <Grid item xs={5} sm={5} md={5}>
                    <Typography variant="body2" className={classes.typography}>
                      Quantity
                    </Typography>
                    <CssTextField
                      size="small"
                      variant="outlined"
                      required
                      fullWidth
                      id="fullname"
                      name="fullname"
                      autoComplete="fullname"
                      autoFocus
                      className={classes.textfield}
                    />
                  </Grid>
                  <Grid item xs={5} sm={5} md={5}>
                    <Typography variant="body2" className={classes.typography}>
                      Price
                    </Typography>
                    <CssTextField
                      size="small"
                      variant="outlined"
                      required
                      fullWidth
                      id="fullname"
                      name="fullname"
                      autoComplete="fullname"
                      autoFocus
                      className={classes.textfield}
                    />
                  </Grid>
                  <Grid item style={{ marginTop: "6px" }} xs={12}>
                    <Button
                      type="submit"
                      fullWidth
                      variant="contained"
                      style={{ backgroundColor: "#FFEFE5", color: "#FF5C00" }}
                      className={classes.submit}
                    >
                      ADD VARIANT
                    </Button>
                    <Button
                      type="submit"
                      fullWidth
                      variant="contained"
                      style={{
                        backgroundColor: "white",
                        color: "#FF5C00",
                        border: "1px solid #FF5C00"
                      }}
                      className={classes.submit}
                    >
                      DELETE
                    </Button>
                  </Grid>
                </Grid>
              </Grid>

              <Grid
                item
                xs={12}
                sm={3}
                md={4}
                style={{ display: "flex" }}
                className={classes.addVariantButton}
              >
                <Grid
                  container
                  className={`${classes.paper}`}
                  justify="center"
                  alignItems="center"
                  style={{
                    backgroundColor: "#FAFAFA",
                    borderRadius: "20px",
                    cursor: "pointer"
                  }}
                >
                  <AddIcon style={{ opacity: "70%" }} />
                  Add Variant
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </>
  );
}
