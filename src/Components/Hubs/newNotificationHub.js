import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import Alert from "@material-ui/lab/Alert";
import Container from "@material-ui/core/Container";
import NotificationsNoneIcon from "@material-ui/icons/NotificationsNone";
import Badge from "@material-ui/core/Badge";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import Link from "@material-ui/core/Link";
import Box from "@material-ui/core/Box";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import Typography from "@material-ui/core/Typography";
import { withStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import Avatar from "@material-ui/core/Avatar";
import FiberManualRecordIcon from "@material-ui/icons/FiberManualRecord";
import Subheader from "../Subheader/Subheader";

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1
  },
  mainContainer: {
    display: "flex"
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(2),
    fontSize: "14px"
  },
  paper: {
    marginTop: "20px",

    background: "#fff",
    padding: "10px 0 0 0"
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
    fontSize: "20px"
  },
  textfield: {
    minWidth: "100%",
    marginLeft: 0,
    backgroundColor: "#FAFAFA",
    borderRadius: "5px"
  },
  typography: {
    float: "left",
    fontSize: "14px",
    fontWeight: "500",
    margin: "0 0 0 0",
    color: "black"
  },
  icon: {
    color: "#FF5C00",
    width: "9px",
    height: "9px"
  }
}));
const CssTextField = withStyles({
  root: {
    "& label.Mui-focused": {
      color: "grey",
      fontSize: 10
    },
    "& .MuiInput-underline:after": {
      borderBottomColor: "green"
    },
    "& .MuiOutlinedInput-root": {
      "& fieldset": {
        borderColor: "grey",
        opacity: "60%"
      },
      "&:hover fieldset": {
        borderColor: "#FF5C00"
      },
      "&.Mui-focused fieldset": {
        borderColor: "#FF5C00"
      }
    }
  }
})(TextField);
export default function SettingsHub() {
  const classes = useStyles();

  return (
    <>
      <Grid container className={classes.mainContainer}>
        <Grid item md={12} sm={12} xs={12}>
          <Alert
            icon={false}
            severity="warning"
            style={{
              backgroundColor: "#FAEAE1",
              border: "1px solid #FF5C00"
            }}
          >
            Your store is not visible yet, go to settings to set up your store
          </Alert>
        </Grid>

        <Subheader name="Notifications" />

        <Grid xs={12} sm={12} md={12}>
          <Paper style={{ padding: "10px 0 10px 0" }}>
            <List className={classes.root}>
              <ListItem>
                <ListItemAvatar>
                  <FiberManualRecordIcon className={classes.icon} />
                </ListItemAvatar>
                Your store store is not set up yet, go to Settings to Create
                your store
              </ListItem>

              <ListItem>
                <ListItemAvatar>
                  <FiberManualRecordIcon className={classes.icon} />
                </ListItemAvatar>
                Your bank account details has not been completed go to setting
                to fill your details
              </ListItem>
              <ListItem>
                <ListItemAvatar>
                  <FiberManualRecordIcon className={classes.icon} />
                </ListItemAvatar>
                Welcome to Seller Vasiti, remember to complete your profile so
                you can start selling with us today!
              </ListItem>
            </List>
          </Paper>
        </Grid>
      </Grid>
    </>
  );
}
