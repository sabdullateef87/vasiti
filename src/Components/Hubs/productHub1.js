import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import Pagination from "@material-ui/lab/Pagination";
import SingleWholesalePageHeader from "../wholesalers/singleWholesalePageHeader";

import { useState, useEffect } from "react";
import Button from "@material-ui/core/Button";
import axios from "axios";



const useStyles = makeStyles(theme => ({
  root: {
    width: "100%",
    marginTop: theme.spacing(3),
    overflowX: "auto"
  },
  table: {
    minWidth: 650
  },
  button: {
    margin: "3px",
    backgroundColor: "#FF5C00",
    color: "white"
  }
}));

export default function SingleWholesaler() {
  const [wholesaler, setWholesaler] = useState([
    {
      id: 2,
      firstname: "suleiman",
      lastname: "abdullateef",
      email: "sabdu@gmail.com",
      address_line: "Agodosegun",
      city: "Ikorodu",
      state: "Lagos",
      country: "Nigeria",
      phone: "0802334564774"
    }
  ]);
  const [product, setProduct] = useState([
    {
      image: "",
      color: "red",
      size: 24,
      quantity: 25,
      price: "#5000",
      stock_status: ""
    },
    {
      image: "",
      color: "white",
      size: 30,
      quantity: 8,
      price: "#5000",
      stock_status: ""
    },
    {
      image: "",
      color: "red",
      size: 24,
      quantity: 25,
      price: "#5000",
      stock_status: ""
    },
    {
      image: "",
      color: "red",
      size: 24,
      quantity: 25,
      price: "#5000",
      stock_status: ""
    },
    {
      image: "",
      color: "red",
      size: 24,
      quantity: 25,
      price: "#5000",
      stock_status: ""
    },
    {
      image: "",
      color: "red",
      size: 24,
      quantity: 25,
      price: "#5000",
      stock_status: ""
    },
    {
      image: "",
      color: "red",
      size: 24,
      quantity: 25,
      price: "#5000",
      stock_status: ""
    },
    {
      image: "",
      color: "red",
      size: 24,
      quantity: 25,
      price: "#5000",
      stock_status: ""
    },
    {
      image: "",
      color: "red",
      size: 24,
      quantity: 25,
      price: "#5000",
      stock_status: ""
    },
    {
      image: "",
      color: "red",
      size: 24,
      quantity: 25,
      price: "#5000",
      stock_status: ""
    },
    {
      image: "",
      color: "red",
      size: 24,
      quantity: 25,
      price: "#5000",
      stock_status: ""
    },
    {
      image: "",
      color: "red",
      size: 24,
      quantity: 25,
      price: "#5000",
      stock_status: ""
    },
    {
      image: "",
      color: "red",
      size: 24,
      quantity: 25,
      price: "#5000",
      stock_status: ""
    },
    {
      image: "",
      color: "red",
      size: 24,
      quantity: 25,
      price: "#5000",
      stock_status: ""
    },
    {
      image: "",
      color: "red",
      size: 24,
      quantity: 25,
      price: "#5000",
      stock_status: ""
    },
    {
      image: "",
      color: "red",
      size: 24,
      quantity: 25,
      price: "#5000",
      stock_status: ""
    },
    {
      image: "",
      color: "red",
      size: 24,
      quantity: 25,
      price: "#5000",
      stock_status: ""
    },
    {
      image: "",
      color: "red",
      size: 24,
      quantity: 25,
      price: "#5000",
      stock_status: ""
    },
    {
      image: "",
      color: "red",
      size: 24,
      quantity: 25,
      price: "#5000",
      stock_status: ""
    }
  ]);

  const [products, setProducts] = useState([]);
  const [loading, setLoading] = useState(false);

  const [currentpage, setCurrentPage] = useState(1);
  const [productsPerPage, setProductsPerPage] = useState(5);

  useEffect(() => {
    const fetchProduct = async () => {
      setLoading(true);
      const res = await axios.get("/jdjhxfjhjkhjksd");
      setProducts(res.data);
      setLoading(false);
    };
    fetchProduct();
  }, []);

  const indexOfLastProduct = productsPerPage * currentpage;
  const indexOfFirstProduct = indexOfLastProduct - productsPerPage;
  const currentProduct = product.slice(indexOfFirstProduct, indexOfLastProduct);
  //const currentProduct = products.slice(indexOfFirstProduct, indexOfLastProduct);
  const paginate = (event, value) => setCurrentPage(value);

  const classes = useStyles();

  return (
    <>
      <SingleWholesalePageHeader product={product} />
      <div style={{ padding: "30px" }}>
        <Paper className={classes.root}>
          <Table className={classes.table}>
            <TableHead>
              <TableRow>
                <TableCell>Image</TableCell>
                <TableCell align="center">
                  <strong>Color</strong>
                </TableCell>
                <TableCell align="center">
                  <strong>Size&nbsp;</strong>
                </TableCell>
                <TableCell align="center">
                  <strong>&nbsp;Qty</strong>
                </TableCell>
                <TableCell align="center">
                  <strong>&nbsp;Price</strong>
                </TableCell>
                <TableCell align="center">
                  <strong>Stock Status&nbsp;</strong>
                </TableCell>
                <TableCell align="center">
                  <strong>Action&nbsp;</strong>
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {currentProduct.map(row => (
                <TableRow key={row.image}>
                  <TableCell component="th" scope="row">
                    {row.image}
                  </TableCell>
                  <TableCell align="center">{row.color}</TableCell>
                  <TableCell align="center">{row.size}</TableCell>
                  <TableCell align="center">{row.quantity}</TableCell>
                  <TableCell align="center">{row.price}</TableCell>
                  <TableCell align="center">{row.stock_status}</TableCell>
                  <TableCell align="center">
                    <Button variant="contained" className={classes.button}>
                      Edit
                    </Button>
                    <Button variant="contained" className={classes.button}>
                      Remove
                    </Button>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </Paper>
      </div>
      <Pagination
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          margin: "40px 0 0 0"
        }}
        count={Math.ceil(product.length / productsPerPage)}
        shape="rounded"
        page={currentpage}
        onChange={paginate}
      />
    </>
  );
}
