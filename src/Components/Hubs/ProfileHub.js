import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import Alert from "@material-ui/lab/Alert";
import Button from "@material-ui/core/Button";
import CreateIcon from "@material-ui/icons/Create";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import Typography from "@material-ui/core/Typography";
import Subheader from "../Subheader/Subheader";

const useStyles = makeStyles(theme => ({
  paper: {
    display: "flex",
    flexDirection: "column",
    background: "#fff",
    marginTop: "20px",
    minHeight: "440px",
    "&.MuiPaper-elevation1": {
      boxShadow: "1px 2px 4px rgba(0,0,0,0.1)",
      borderRadius: "20px"
    },
    [theme.breakpoints.down("xs")]: {
      padding: "0px 20px 40px 20px !important"
    }
  },
  mainContainer: {
    flexGrow: 1,
    display: "flex"
  },

  avatar: {
    margin: theme.spacing(3),
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1)
  },
  submit: {
    margin: theme.spacing(3, 0, 2)
  },
  textfield: {
    minWidth: "100%",
    marginLeft: 0,
    backgroundColor: "#FAFAFA",
    borderRadius: "5px"
  },
  typography: {
    [theme.breakpoints.down("xs")]: {
      fontSize: "14px !important"
    }
  },
  logo: {
    [theme.breakpoints.down("xs")]: {
      width: "50px !important",
      height: "50px !important"
    }
  },
  button: {
    "&.MuiButton-contained": {
      boxShadow: "none"
    },
    backgroundColor: "#FF5C00",
    color: "white"
  },
  buttonContainer: {
    display: "flex",
    justifyContent: "flex-end",
    [theme.breakpoints.down("xs")]: {
      display: "flex",
      justifyContent: "center !important"
    }
  },
  listItem: {
    "&.MuiListItem-gutters": {
      marginButton: "20px !important"
    }
  },
  details: {
    [theme.breakpoints.down("xs")]: {
      margin: "0px !important"
    }
  },
  listItemText: {
    "& .MuiTypography-body1": {
      fontWeight: 500
    },
    marginButtom: "20px"
  },
  userName: {
    fontSize: "24px",
    fontWeight: "500",
    margin: "30px 0 28px 0",
    [theme.breakpoints.down("xs")]: {
      fontSize: "16px",
      marginTop: "0px !important"
    }
  },
  listContainer: {
    marginTop: "13px",
    marginLeft: "40px",
    [theme.breakpoints.down("xs")]: {
      marginLeft: "40px !important",
      marginTop: "0px !important"
    },
    [theme.breakpoints.between("sm", "md")]: {
      marginLeft: "70px !important"
    }
  },
  topButton: {
    [theme.breakpoints.down("xs")]: {
      display: "none !important"
    }
  },
  bottomButton: {
    marginTop: "10px",
    [theme.breakpoints.up("sm")]: {
      display: "none !important"
    }
  }
}));

export default function ProfileHub() {
  const classes = useStyles();

  return (
    <>
      <>
        <Grid container className={classes.mainContainer}>
          <Grid item md={12} sm={12} xs={12}>
            <Alert
              icon={false}
              severity="warning"
              style={{
                backgroundColor: "#FAEAE1",
                border: "1px solid #FF5C00"
              }}
            >
              Your store is not visible yet, go to settings to set up your store
            </Alert>
          </Grid>
          <Subheader name="Profile" />
          <Grid container>
            <Grid item xs={12} md={12} sm={12}>
              <Paper
                className={classes.paper}
                style={{ padding: "41px 61px 98px 71px" }}
              >
                {/* ANCHOR  button Grid */}
                <Grid
                  item
                  className={` ${classes.buttonContainer} ${classes.topButton}`}
                  xs={12}
                  md={12}
                  sm={12}
                >
                  <Button
                    variant="contained"
                    className={classes.button}
                    startIcon={<CreateIcon />}
                  >
                    Edit Profile
                  </Button>
                </Grid>
                {/* ANCHOR  button Grid */}
                {/* ----------------------------------------------------------- */}

                <Grid container justfy="flex-start" alignItems="flex-start">
                  {/* ANCHOR Logo Grid */}
                  <Grid
                    item
                    xs={12}
                    sm={1}
                    md={1}
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      alignItems: "center"
                    }}
                  >
                    <div
                      className={classes.logo}
                      style={{
                        backgroundColor: "#FAEAE1",
                        width: "95px",
                        height: "95px",
                        borderRadius: "100%",
                        color: "#FF5C00",
                        display: "flex",
                        justifyContent: "center",
                        alignItems: "center",
                        marginTop: "1.5em"
                      }}
                    >
                      <Typography className={classes.typography} variant="h3">
                        S
                      </Typography>
                    </div>
                  </Grid>
                  {/* ANCHOR Logo Grid */}
                  {/* ----------------------------------------------------------- */}
                  {/* ANCHOR List Grid */}
                  <Grid
                    item
                    xs={8}
                    sm={8}
                    md={8}
                    className={classes.listContainer}
                  >
                    <List
                      style={{
                        width: "100%"
                      }}
                    >
                      <ListItem className={classes.listItem}>
                        <div>
                          <span>
                            <h1 className={classes.userName}>SUNDAY AYOADE</h1>
                          </span>
                        </div>
                      </ListItem>
                      <ListItem className={classes.listItem}>
                        <div>
                          <span>Phone</span>
                          <ListItemText
                            primary="+234 789 678 678"
                            className={classes.listItemText}
                          />
                        </div>
                      </ListItem>
                      <ListItem className={classes.listItem}>
                        <div>
                          <span>Campus</span>
                          <ListItemText
                            primary="University of lagos"
                            className={classes.listItemText}
                          />
                        </div>
                      </ListItem>
                      <ListItem className={classes.listItem}>
                        <div>
                          <span>Address</span>
                          <ListItemText
                            primary="No 3, Abule Oja, Akoka."
                            className={classes.listItemText}
                          />
                        </div>
                      </ListItem>
                    </List>
                  </Grid>
                </Grid>

                {/* ANCHOR  button Grid */}
                <Grid
                  item
                  className={` ${classes.buttonContainer} ${classes.bottomButton}`}
                  xs={12}
                  md={12}
                  sm={12}
                >
                  <Button
                    variant="contained"
                    className={classes.button}
                    startIcon={<CreateIcon />}
                  >
                    Edit Profile
                  </Button>
                </Grid>
                {/* ANCHOR  button Grid */}
              </Paper>
            </Grid>
          </Grid>
        </Grid>
      </>
    </>
  );
}
