import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import Alert from "@material-ui/lab/Alert";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";

import icon1 from "../wholesalers/icons/Frame.svg";
import icon2 from "../wholesalers/icons/Group 1.svg";
import icon3 from "../wholesalers/icons/returned.svg";
import Subheader from "../Subheader/Subheader";

const useStyles = makeStyles(theme => ({
  paper: {
    display: "flex",
    background: "#fff",
    padding: "30px 20px 0 20px",
    margin: "0 20px 20px 0 ",
    "&.MuiPaper-elevation1": {
      boxShadow: "1px 2px 4px rgba(0,0,0,0.1)",
      borderRadius: "10px"
    },
    [theme.breakpoints.down("xs")]: {
      marginRight: "0px !important"
    }
  },
  mainContainer: {
    display: "flex",
    backgroundColor: "#E5E5E5"
  },

  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1)
  },
  submit: {
    margin: theme.spacing(3, 0, 2)
  },
  welcome: {
    [theme.breakpoints.down("xs")]: {
      fontSize: "24px !important"
    }
  },
  textfield: {
    minWidth: "100%",
    marginLeft: 0,
    backgroundColor: "#FAFAFA",
    borderRadius: "5px"
  },
  typography: {
    float: "left",
    fontSize: "14px",
    fontWeight: "500",
    margin: "0 0 0 0",
    color: "black"
  },
  button: {
    "&.MuiButton-contained": {
      boxShadow: "none"
    }
  },
  buttonContainer: {
    marginTop: "30px",
    [theme.breakpoints.down("xs")]: {
      padding: "0 0 20px 0 !important",
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
      marginTop: "10px !important"
    }
  }
}));

export default function SellerHub() {
  const classes = useStyles();

  return (
    <>
      <>
        <Grid container className={classes.mainContainer}>
          <Grid item md={12} sm={12} xs={12}>
            <Alert
              icon={false}
              severity="warning"
              style={{
                backgroundColor: "#FAEAE1",
                border: "1px solid #FF5C00"
              }}
            >
              Your store is not visible yet, go to settings to set up your store
            </Alert>
          </Grid>
          <Subheader name="Home" />

          <Grid item xs={12} sm={12} md={12}>
            <Typography
              className={classes.welcome}
              style={{
                float: "left",
                fontSize: "32px",
                fontWeight: "400",
                opacity: "80%"
              }}
            >
              Welcome back Sunday !
            </Typography>
          </Grid>

          <Grid item xs={12} sm={3} md={2}>
            <Paper className={classes.paper} style={{ height: "100px" }}>
              <Grid item xs={6} sm={6} md={6}>
                <div
                  style={{
                    fontWeight: "500",
                    textAlign: "left"
                  }}
                >
                  Products
                  <br /> 0
                </div>
              </Grid>
              <Grid item xs={6} sm={6} md={6}>
                <div
                  style={{
                    float: "right"
                  }}
                >
                  <img src={icon1} alt="products icon" />
                </div>
              </Grid>
            </Paper>
          </Grid>

          <Grid item xs={12} sm={3} md={2}>
            <Paper className={classes.paper} style={{ height: "100px" }}>
              <Grid item xs={6} sm={6} md={6}>
                <div
                  style={{
                    fontWeight: "500",

                    textAlign: "left"
                  }}
                >
                  Sold
                  <br /> 0
                </div>
              </Grid>
              <Grid item xs={6} sm={6} md={6}>
                <div
                  style={{
                    float: "right"
                  }}
                >
                  <img src={icon2} alt="products icon" />
                </div>
              </Grid>
            </Paper>
          </Grid>

          <Grid item xs={12} sm={3} md={2}>
            <Paper className={classes.paper} style={{ height: "100px" }}>
              <Grid item xs={6} sm={6} md={6}>
                <div
                  style={{
                    fontWeight: "500",

                    textAlign: "left"
                  }}
                >
                  Returned <br />0
                </div>
              </Grid>
              <Grid item xs={6} sm={6} md={6}>
                <div
                  style={{
                    float: "right"
                  }}
                >
                  <img src={icon3} alt="products icon" />
                </div>
              </Grid>
            </Paper>
          </Grid>

          <Grid item xs={12} sm={12} md={12}>
            <Paper
              className={`${classes.paper} ${classes.buttonContainer}`}
              style={{
                height: "200px",
                display: "flex",
                alignItems: "flex-end",
                padding: "0 0 50px 50px"
              }}
            >
              <Button
                className={classes.button}
                variant="contained"
                style={{
                  backgroundColor: "#FF5C00",
                  color: "white",
                  boxShadow: "none"
                }}
              >
                Go to Wholesale Market
              </Button>
            </Paper>
          </Grid>
        </Grid>
      </>
    </>
  );
}
