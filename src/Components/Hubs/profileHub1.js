import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import Alert from "@material-ui/lab/Alert";
import Container from "@material-ui/core/Container";
import NotificationsNoneIcon from "@material-ui/icons/NotificationsNone";
import Badge from "@material-ui/core/Badge";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import { withStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import { FormControl, MenuItem, Select } from "@material-ui/core";
import Subheader from "../Subheader/Subheader";

const useStyles = makeStyles(theme => ({
  paper: {
    marginTop: "40px",
    display: "contents",

    background: "#fff",
    padding: "10px 0 0 0"
  },
  mainContainer: {
    flexGrow: 1,
    backgroundColor: "#E4E4E4"
  },

  avatar: {
    margin: theme.spacing(3),
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    // Fix IE 11 issue.
    marginTop: theme.spacing(1)
  },
  submit: {
    margin: theme.spacing(3, 0, 2)
  },
  textfield: {
    marginLeft: 0,
    backgroundColor: "#FAFAFA",
    borderRadius: "5px"
  },
  typography: {
    float: "left",
    fontSize: "14px",
    fontWeight: "500",
    margin: "0 0 0 0",
    color: "black"
  },
  logo: {
    marginTop: "-10px",
    [theme.breakpoints.down("xs")]: {
      width: "24px !important",
      height: "24px !important",
      marginTop: "-1.9px !important"
    }
  },
  accountDetails: {
    [theme.breakpoints.down("xs")]: {
      fontSize: "14px !important",
      marginTop: "50px !important"
    }
  },
  name: {
    fontWeight: "bold",
    marginLeft: "10px",
    [theme.breakpoints.down("sm")]: {
      fontWeight: 600,
      fontSize: "14px !important",
      marginLeft: "5px !important"
    }
  },
  profileGrid: {
    [theme.breakpoints.down("sm")]: {
      display: "none"
    }
  },
  formContainer: {
    [theme.breakpoints.down("xs")]: {
      padding: "21px !important"
    }
  },
  formControl: {
    margin: "-20px 0 -10px 0",
    minWidth: "100%",
    backgroundColor: "#FAFAFA",
    borderBottom: "none !important",
    borderRadius: "5px",
    "& .MuiInput-underline:hover:not(.Mui-disabled):before": {
      borderBottom: "none"
    },
    "& .MuiInput-underline:after": {
      borderBottom: "none !important"
    },
    "& .MuiInput-underline:before": {
      borderBottom: "none !important"
    }
  },
  select: {
    "& .MuiSelect-selectMenu": {
      height: "1.185rem"
    },
    "& .MuiInputBase-input": {
      padding: "12px 14px 7px 14px",
      display: "flex"
    }
  }
}));
const CssTextField = withStyles({
  root: {
    "& label.Mui-focused": {
      color: "grey",
      fontSize: 10
    },
    "& ::placeholder": { fontSize: "14px" },

    "&:hover": {
      borderColor: "#FF5C00"
    },
    "& .MuiInput-underline:after": {
      borderBottom: "none !important"
    },

    "& .MuiInput-underline:before": {
      borderColor: "#FF5C00"
    },
    "& .MuiInput-underline:hover:not(.Mui-disabled):before": {
      borderColor: "#FF5C00"
    },
    "& .MuiOutlinedInput-root": {
      "& fieldset": {
        border: "none",
        opacity: "60%"
      },
      "&:hover fieldset": {
        borderColor: "#FF5C00"
      },
      "&.Mui-focused fieldset": {
        border: "1px solid #FF5C00"
      }
    }
  }
})(TextField);
export default function ProfileHub1() {
  const classes = useStyles();

  return (
    <>
      {/* <CssBaseline /> */}
      <Grid container className={classes.mainContainer}>
        <Grid item xs={12}>
          <Alert
            icon={false}
            severity="warning"
            style={{
              backgroundColor: "#FAEAE1",
              border: "1px solid #FF5C00",
              boxShadow: "none"
            }}
          >
            Your store is not visible yet, go to settings to set up your store
          </Alert>
        </Grid>
        <Subheader name="Profile" />

        <Grid
          className={classes.formContainer}
          container
          style={{
            width: "100%",
            minHeight: "200px",
            backgroundColor: "white",
            borderRadius: "20px",
            padding: "30px 80px 30px 80px",
            marginTop: "10px"
          }}
        >
          <Grid item xs={12} md={12} sm={12} style={{ display: "flex" }}>
            <h1 style={{ fontWeight: "500", fontSize: "24px" }}>
              Edit Profile
            </h1>
          </Grid>
          <Grid item xs={12} md={6} sm={12}>
            <form className={classes.form} noValidate>
              <Typography variant="body2" className={classes.typography}>
                Full Name
              </Typography>
              <CssTextField
                size="small"
                variant="outlined"
                margin="normal"
                required
                fullWidth
                id="fullname"
                name="fullname"
                autoComplete="fullname"
                autoFocus
                className={classes.textfield}
              />
              <Typography variant="body2" className={classes.typography}>
                Phone Number
              </Typography>
              <CssTextField
                size="small"
                variant="outlined"
                margin="normal"
                required
                fullWidth
                name="phone"
                type="phone"
                id="phone"
                autoComplete="phone"
                className={classes.textfield}
              />
              <Typography variant="body2" className={classes.typography}>
                Email
              </Typography>
              <CssTextField
                size="small"
                variant="outlined"
                margin="normal"
                required
                fullWidth
                id="email"
                name="email"
                autoComplete="email"
                autoFocus
                className={classes.textfield}
              />
              <Typography variant="body2" className={classes.typography}>
                Bank Name
              </Typography>
              <CssTextField
                size="small"
                variant="outlined"
                margin="normal"
                required
                fullWidth
                id="fullname"
                name="fullname"
                autoComplete="fullname"
                autoFocus
                className={classes.textfield}
              />
              <Grid item xs={12} sm={12} style={{ marginBottom: "15px" }}>
                <Typography
                  variant="body2"
                  className={classes.typography}
                  style={{ marginBottom: "5px" }}
                >
                  Select Campus
                </Typography>
                <FormControl
                  size="small"
                  className={classes.formControl}
                  style={{ marginTop: "0.9px", marginLeft: "-2px" }}
                >
                  <Select
                    labelId="institution"
                    id="demo-simple-select-outlined"
                    name="institution"
                    // value={values.institution}
                    // onChange={handleChange}
                    // labelWidth={labelWidth}
                    className={classes.select}
                  >
                    <MenuItem value="">
                      <em>None</em>
                    </MenuItem>
                    <MenuItem value="convenant">Convenat</MenuItem>
                    <MenuItem value="University of Lagos">
                      University of Lagos
                    </MenuItem>
                    <MenuItem value="University of Ibadan">
                      University of Ibadan
                    </MenuItem>
                  </Select>
                </FormControl>
              </Grid>
              <Typography variant="body2" className={classes.typography}>
                Address
              </Typography>
              <CssTextField
                size="small"
                variant="outlined"
                margin="normal"
                required
                fullWidth
                id="address"
                name="address"
                autoComplete="address"
                autoFocus
                className={classes.textfield}
              />

              <Button
                type="submit"
                fullWidth
                variant="contained"
                style={{ backgroundColor: "#FF5C00", color: "white" }}
                className={classes.submit}
              >
                SAVE CHANGES
              </Button>
            </form>
          </Grid>
          <Grid
            item
            xs={12}
            md={6}
            sm={6}
            style={{
              display: "flex",
              justifyContent: "flex-end",
              alignItems: "center"
            }}
          >
            <div
              className={classes.profileGrid}
              style={{
                width: "400px",
                height: "400px",
                backgroundColor: "#FAEAE1"
              }}
            ></div>
          </Grid>
        </Grid>
      </Grid>
    </>
  );
}
