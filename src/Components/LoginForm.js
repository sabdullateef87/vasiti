import React from "react";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import Link from "@material-ui/core/Link";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import { withStyles } from "@material-ui/core/styles";
import Hidden from "@material-ui/core/Hidden";
import useMediaQuery from "@material-ui/core/useMediaQuery";

import backgroundImage from "./signupimage.png";
import Logo from "../Components/wholesalers/icons/Vasiti-Logo-black 2@1x.png";
import Logo2 from "../Components/wholesalers/icons/Vasiti-Logo-black 1.png";
import useLoginForm from "../CustonHooks/useLoginForm";
import validateLoginForm from "../Functions/LoginValidate";

const CssTextField = withStyles({
  root: {
    "& label.Mui-focused": {
      color: "grey",
      fontSize: 10
    },
    "& ::placeholder": { fontSize: "14px" },

    "&:hover": {
      borderColor: "#FF5C00"
    },
    "& .MuiInput-underline:after": {
      borderBottom: "none !important"
    },
    "& .MuiInput-underline:before": {
      borderColor: "#FF5C00"
    },
    "& .MuiInput-underline:hover:not(.Mui-disabled):before": {
      borderColor: "#FF5C00"
    },
    "& .MuiOutlinedInput-root": {
      "& fieldset": {
        border: "none",
        opacity: "60%"
      },
      "&:hover fieldset": {
        borderColor: "#FF5C00"
      },
      "&.Mui-focused fieldset": {
        border: "1px solid #FF5C00"
      }
    }
  }
})(TextField);

const useStyles = makeStyles(theme => ({
  paper: {
    marginTop: "20px",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    background: "#fff",
    padding: "64px 10px 53px 10px"
  },
  mainContainer: {
    background: `url(${backgroundImage})`,
    maxWidth: "100vw",
    minHeight: "100vh",
    backgroundRepeat: "no-repeat",
    backgroundSize: "cover",
    backgroundPosition: "center",
    padding: "10px 0 20px 0",
    display: "flex",
    "@media only screen and (max-width: 767px)": {
      backgroundImage: "none",
      backgroundColor: "#E5E5E5",
      padding: "0 5px",
      height: "630px"
    }
  },

  logo: {
    "@media only screen and (max-width: 768px)": {
      marginButtom: "0px"
    }
  },
  avatar: {
    margin: theme.spacing(3),
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1)
  },
  submit: {
    // margin: theme.spacing(3, 0, 2),
    fontSize: "20px",
    [theme.breakpoints.down("xs")]: {
      fontSize: "14px !important"
    }
  },
  textfield: {
    minWidth: "100%",
    marginLeft: 0,
    backgroundColor: "#FAFAFA",
    borderRadius: "5px",
    height: "3.125rem"
  },
  typography: {
    float: "left",
    fontSize: "14px",
    fontWeight: "500",
    margin: "0 0 0 0",
    color: "black"
  },
  supergrid: {
    "@media screen and (max-width: 1024px) and (min-width: 768px)": {
      marginTop: "40px"
    },
    "@media only screen and (min-width: 960px)": {
      paddingTop: "120px",
      paddingLeft: "-50px"
    }
  },
  logoParent: {
    "@media only screen and (max-width: 768px)": {
      paddingLeft: "-2rem"
    }
  }
}));

export default function SignIn() {
  const classes = useStyles();
  const matches = useMediaQuery("(min-width:768px)");

  const USER_INFO = {
    pageFrom: "vendor",
    email: "",
    password: ""
  };
  const [
    values,
    handleChange,
    handleSubmit,
    errors,
    handleValidate
  ] = useLoginForm(USER_INFO, validateLoginForm);
  const errorMessageStyle = {
    color: "red",
    opacity: "70%",
    fontSize: "10px",
    fontWidth: "bolder",
    fontStyle: "oblique"
  };
  return (
    <>
      <Grid container className={classes.mainContainer}>
        <Grid
          className={classes.logoParent}
          item
          md={6}
          sm={12}
          xs={12}
          style={{
            padding: matches ? "50px 0 0 8rem" : "0 0 0 4rem",
            textAlign: "left"
          }}
        >
          <img
            className={classes.logoParent}
            src={matches ? Logo : Logo2}
            alt="logo"
            style={{
              position: "relative",
              top: "30px"
            }}
          />
          {/* takimg welcome text */}
          <Hidden smDown>
            <h1
              style={{
                fontSize: "48px",
                color: "white",
                display: "flex",
                alignContent: "flex-end",
                position: "relative",
                top: "20%"
              }}
            >
              Welcome to
              <br />
              Vasiti Seller Hub.
            </h1>
          </Hidden>
        </Grid>
        <Grid item md={6} sm={12} xs={12} className={classes.supergrid}>
          <Grid item xs={12} sm={12} md={12}>
            <Container
              component="main"
              maxWidth="xs"
              style={{ backgroundColor: "#fff", borderRadius: "10px" }}
            >
              <CssBaseline />
              <div className={classes.paper}>
                <Typography
                  component="h1"
                  variant="h5"
                  style={{
                    color: "#FF5C00",
                    fontWeight: "bolder",
                    margin: "5px 0 5px 0"
                  }}
                >
                  Sign in
                </Typography>
                <form
                  className={classes.form}
                  noValidate
                  onSubmit={handleSubmit}
                >
                  <div>
                    <Typography variant="body2" className={classes.typography}>
                      Email
                    </Typography>
                    <CssTextField
                      className={classes.textfield}
                      /// size="small"
                      variant="outlined"
                      placeholder="Enter your email"
                      margin="normal"
                      required
                      fullWidth
                      id="email"
                      name="email"
                      value={values.email}
                      onChange={handleChange}
                      onKeyUp={handleValidate}
                      autoComplete="email"
                    />

                    <span>
                      {errors.email && (
                        <span style={errorMessageStyle}>{errors.email}</span>
                      )}
                    </span>
                  </div>
                  <Typography variant="body2" className={classes.typography}>
                    Password
                  </Typography>
                  <CssTextField
                    className={classes.textfield}
                    //size="small"
                    variant="outlined"
                    placeholder="Enter your password"
                    margin="normal"
                    required
                    fullWidth
                    type="password"
                    id="password"
                    name="password"
                    value={values.password}
                    onChange={handleChange}
                    onKeyUp={handleValidate}
                  />
                  <div style={{ marginBottom: "5px" }}>
                    {errors.password && (
                      <span style={errorMessageStyle}>{errors.password}</span>
                    )}
                  </div>

                  <Button
                    type="submit"
                    fullWidth
                    variant="contained"
                    style={{ backgroundColor: "#FF5C00", color: "white" }}
                    className={classes.submit}
                  >
                    Sign In
                  </Button>
                  <Grid
                    container
                    justify="center"
                    style={{ marginTop: "20px" }}
                  >
                    <Grid item xs>
                      <Link href="/forgotpassword" variant="body2">
                        Forgot password?
                      </Link>
                    </Grid>
                    <Grid item>
                      <Link href="/signup" variant="body2">
                        {"Don't have an account? Sign Up"}
                      </Link>
                    </Grid>
                  </Grid>
                </form>
              </div>
            </Container>
          </Grid>
        </Grid>
      </Grid>
    </>
  );
}
