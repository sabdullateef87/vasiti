const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;

const forgotPasswordValidate = values => {
  let errors = {};

  //email Validation
  if (!values.email) {
    errors.email = "Required";
  } else if (!emailRegex.test(values.email)) {
    errors.email = "Invalid";
  }

  return errors;
};

export default forgotPasswordValidate;
