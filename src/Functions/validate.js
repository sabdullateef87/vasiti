const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
const passwordRegex = /(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[#$^+=!*()@%&]).{7,10}/;
const nameRegex = /^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*$/;
const validateForm = values => {
  let errors = {};
  //Firstname validation
  if (
    !values.firstname ||
    values.firstname.length === 0 ||
    values.firstname === ""
  ) {
    errors.firstname = "Required";
  } else if (!nameRegex.test(values.firstname)) {
    errors.firstname = "Invalid";
  }

  //Lastname Validation
  if (!values.lastname) {
    errors.lastname = "Required";
  } else if (!nameRegex.test(values.lastname)) {
    errors.lastname = "Invalid";
  }

  //phonenumber validation
  if (!values.phone) {
    errors.phone = "Required";
  } else if (values.phone.length < 11) {
    errors.phone = "Number too short";
  }

  //email Validation
  if (!values.email) {
    errors.email = "Required";
  } else if (!emailRegex.test(values.email)) {
    errors.email = "Invalid";
  }
  //password validation
  if (!values.password) {
    errors.password = "Required";
  } else if (!passwordRegex.test(values.password)) {
    errors.password =
      "password must be greater than 8 characters and e.g (1Abdoliumytu@)";
  }
  // else if (values.password.length < 6) {
  //   errors.password = "password must be more than 6 characters";
  // }
  //confirm_password validation
  if (!values.confirm_password) {
    errors.confirm_password = "Required";
  } else if (!passwordRegex.test(values.confirm_password)) {
    errors.confirm_password =
      "password must be greater than 8 characters and e.g (1Abdoliumytu@)";
  }
  // else if (values.confirm_password.length < 6) {
  //   errors.confirm_password = "password must be more than 6 characters";
  // }

  //password equality
  if (values.password !== values.confirm_password) {
    errors.equality = "passwords do not match";
  }

  //address line validation
  if (!values.address_line) {
    errors.address_line = "Required";
  }

  //institution

  if (!values.institution) {
    errors.institution = "Required";
  }

  //city validation
  if (!values.city) {
    errors.city = "Required";
  }

  //state validation
  if (!values.state) {
    errors.state = "Required";
  }
  //countr validation
  if (!values.country) {
    errors.country = "Required";
  }

  //campus validation
  if (!values.oncampus) {
    errors.oncampus = "Required";
  }

  return errors;
};

export default validateForm;
