const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
const passwordRegex = /(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[#$^+=!*()@%&]).{8,10}/;

const validateLoginForm = values => {
  let errors = {};

  //email Validation
  if (!values.email) {
    errors.email = "Required";
  } else if (!emailRegex.test(values.email)) {
    errors.email = "Invalid";
  }
  //password validation
  if (!values.password) {
    errors.password = "Required";
  }

  return errors;
};

export default validateLoginForm;
